#ifndef CREDITS2_H
#define CREDITS2_H

#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace credits2
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif