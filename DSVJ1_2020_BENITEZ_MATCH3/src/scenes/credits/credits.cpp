#include "credits.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace credits
	{
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::rightArrowButton.width, (float)texture::rightArrowButton.height }))
			{
				rightArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS2;
				}
			}
			else rightArrowButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Programmer", (screenWidth / 2) - MeasureText("Programmer", sizes::text2 / 2), pos::creditsPosY, sizes::text2, BLACK);
			DrawText("Micaela Luz Benitez", (screenWidth / 2) - MeasureText("Micaela Luz Benitez", sizes::text / 2), pos::creditsPosY2, sizes::text, WHITE);

			DrawText("Artist", (screenWidth / 2) - MeasureText("Artist", sizes::text2 / 2), pos::creditsPosY3, sizes::text2, BLACK);
			DrawText("Micaela Luz Benitez", (screenWidth / 2) - MeasureText("Micaela Luz Benitez", sizes::text / 2), pos::creditsPosY4, sizes::text, WHITE);
			DrawText("Nicolas Jimenez", (screenWidth / 2) - MeasureText("Nicolas Jimenez", sizes::text / 2), pos::creditsPosY5, sizes::text, WHITE);
			DrawText("Lucy Wood from trueachievements.com", (screenWidth / 2) - MeasureText("Lucy Wood from trueachievements.com", sizes::text / 2), pos::creditsPosY6, sizes::text, WHITE);
			DrawText("Impressive Inks from pinterest.cl", (screenWidth / 2) - MeasureText("Impressive Inks from pinterest.cl", sizes::text / 2), pos::creditsPosY7, sizes::text, WHITE);
			DrawText("Cookie from newgrounds.com", (screenWidth / 2) - MeasureText("Cookie from newgrounds.com", sizes::text / 2), pos::creditsPosY8, sizes::text, WHITE);
			DrawText("Alucard from newgrounds.com", (screenWidth / 2) - MeasureText("Alucard from newgrounds.com", sizes::text / 2), pos::creditsPosY9, sizes::text, WHITE);
			DrawText("codeinfernogames from newgrounds.com", (screenWidth / 2) - MeasureText("codeinfernogames from newgrounds.com", sizes::text / 2), pos::creditsPosY10, sizes::text, WHITE);
			DrawText("bytecodeminer from wall.alphacoders.com", (screenWidth / 2) - MeasureText("bytecodeminer from wall.alphacoders.com", sizes::text / 2), pos::creditsPosY11, sizes::text, WHITE);
			DrawText("gameart2d.com", (screenWidth / 2) - MeasureText("gameart2d.com", sizes::text / 2), pos::creditsPosY12, sizes::text, WHITE);
			DrawText("wallpaperbetter.com", (screenWidth / 2) - MeasureText("wallpaperbetter.com", sizes::text / 2), pos::creditsPosY13, sizes::text, WHITE);
			DrawText("wallpaperaccess.com", (screenWidth / 2) - MeasureText("wallpaperaccess.com", sizes::text / 2), pos::creditsPosY14, sizes::text, WHITE);
			DrawText("wallpapercave.com", (screenWidth / 2) - MeasureText("wallpapercave.com", sizes::text / 2), pos::creditsPosY15, sizes::text, WHITE);

			DrawTexture(texture::menuButton, pos::buttonsLeft, pos::buttonsUp, menuButtonStatus);
			DrawTexture(texture::rightArrowButton, pos::buttonsRight, pos::buttonsUp, rightArrowButtonStatus);

			DrawText("V 2.0", pos::versionPosX, pos::versionPosY, sizes::text, GRAY);
		}

		void deinit()
		{

		}
	}
}