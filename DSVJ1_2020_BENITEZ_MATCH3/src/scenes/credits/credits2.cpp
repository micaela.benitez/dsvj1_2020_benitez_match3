#include "credits2.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace credits2
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::leftArrowButton.width, (float)texture::leftArrowButton.height }))
			{
				leftArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS;
				}
			}
			else leftArrowButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Audio", (screenWidth / 2) - MeasureText("Audio", sizes::text2 / 2), pos::creditsPosY3, sizes::text2, BLACK);
			DrawText("remaxin from opengameart.org", (screenWidth / 2) - MeasureText("remaxin from opengameart.org", sizes::text / 2), pos::creditsPosY4, sizes::text, WHITE);
			DrawText("srzsmack from newgrounds.com", (screenWidth / 2) - MeasureText("srzsmack from newgrounds.com", sizes::text / 2), pos::creditsPosY5, sizes::text, WHITE);
			DrawText("Dj-MilK from newgrounds.com", (screenWidth / 2) - MeasureText("Dj-MilK from newgrounds.com", sizes::text / 2), pos::creditsPosY6, sizes::text, WHITE);
			DrawText("coby12388 from freesound.org", (screenWidth / 2) - MeasureText("coby12388 from freesound.org", sizes::text / 2), pos::creditsPosY7, sizes::text, WHITE);
			DrawText("WarcakeStudios from freesound.org", (screenWidth / 2) - MeasureText("WarcakeStudios from freesound.org", sizes::text / 2), pos::creditsPosY8, sizes::text, WHITE);
			DrawText("Josethehedgehog from freesound.org", (screenWidth / 2) - MeasureText("Josethehedgehog from freesound.org", sizes::text / 2), pos::creditsPosY9, sizes::text, WHITE);
			DrawText("euanmj from freesound.org", (screenWidth / 2) - MeasureText("euanmj from freesound.org", sizes::text / 2), pos::creditsPosY10, sizes::text, WHITE);
			DrawText("Jaoreir from freesound.org", (screenWidth / 2) - MeasureText("Jaoreir from freesound.org", sizes::text / 2), pos::creditsPosY11, sizes::text, WHITE);
			DrawText("MEAXX from freesound.org", (screenWidth / 2) - MeasureText("MEAXX from freesound.org", sizes::text / 2), pos::creditsPosY12, sizes::text, WHITE);
			DrawText("LG from freesound.org", (screenWidth / 2) - MeasureText("LG from freesound.org", sizes::text / 2), pos::creditsPosY13, sizes::text, WHITE);
			DrawText("Raclure from freesound.org", (screenWidth / 2) - MeasureText("Raclure from freesound.org", sizes::text / 2), pos::creditsPosY14, sizes::text, WHITE);

			DrawTexture(texture::leftArrowButton, pos::buttonsLeft, pos::buttonsUp, leftArrowButtonStatus);
			DrawTexture(texture::menuButton, pos::buttonsRight, pos::buttonsUp, menuButtonStatus);

			DrawText("V 2.0", pos::versionPosX, pos::versionPosY, sizes::text, GRAY);
		}

		void deinit()
		{

		}
	}
}