#ifndef SETTINGS_H
#define SETTINGS_H

#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace settings
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif