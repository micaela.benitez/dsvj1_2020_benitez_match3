#include "settings.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace settings
	{
		static Color audioButtonStatus = WHITE;
		static Color resolutionButtonStatus = WHITE;
		static Color gameButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{
			texture::audioButton = LoadTexture("res/raw/textures/audiobutton.png");
			texture::audioButton.width = sizes::buttonsWidth;
			texture::audioButton.height = sizes::buttonsHeight;

			texture::resolutionButton = LoadTexture("res/raw/textures/resolutionbutton.png");
			texture::resolutionButton.width = sizes::buttonsWidth;
			texture::resolutionButton.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::settingPosY, (float)texture::audioButton.width, (float)texture::audioButton.height }))
			{
				audioButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::AUDIO;
				}
			}
			else audioButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::settingPosY2, (float)texture::resolutionButton.width, (float)texture::resolutionButton.height }))
			{
				resolutionButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::RESOLUTIONS;
				}
			}
			else resolutionButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				gameButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::previousScene;

				}
			}
			else
			{
				menuButtonStatus = WHITE;
				gameButtonStatus = WHITE;
			}

			if (game::previousScene == game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
			}
			else
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
			}

			initialDraw = false;
			pause = false;

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawTexture(texture::audioButton, pos::middleButtonsPosX, pos::settingPosY, audioButtonStatus);
			DrawTexture(texture::resolutionButton, pos::middleButtonsPosX, pos::settingPosY2, resolutionButtonStatus);

			if (game::previousScene == game::Scene::GAMEPLAY) DrawTexture(texture::gameButton, pos::buttonsRight, pos::buttonsUp, gameButtonStatus);
			else DrawTexture(texture::menuButton, pos::buttonsRight, pos::buttonsUp, menuButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(texture::audioButton);
			UnloadTexture(texture::resolutionButton);
		}
	}
}