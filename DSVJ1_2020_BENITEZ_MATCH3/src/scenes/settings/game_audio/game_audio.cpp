#include "game_audio.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace game_audio
	{
		static Color musicMute = WHITE;
		static Color musicLow = WHITE;
		static Color musicHigh = DARKGRAY;
		static Color soundsMute = WHITE;
		static Color soundsLow = WHITE;
		static Color soundsHigh = DARKGRAY;

		static Color settingsButtonStatus = WHITE;
		static Color gameButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{
			texture::muteButton = LoadTexture("res/raw/textures/mute.png");
			texture::muteButton.width = sizes::buttonsWidth;
			texture::muteButton.height = sizes::buttonsHeight;

			texture::lowButton = LoadTexture("res/raw/textures/low.png");
			texture::lowButton.width = sizes::buttonsWidth;
			texture::lowButton.height = sizes::buttonsHeight;

			texture::highButton = LoadTexture("res/raw/textures/high.png");
			texture::highButton.width = sizes::buttonsWidth;
			texture::highButton.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY2, (float)texture::muteButton.width, (float)texture::muteButton.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = DARKGRAY;
					musicLow = WHITE;
					musicHigh = WHITE;
					audio::musicVolume = 0.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY3, (float)texture::lowButton.width, (float)texture::lowButton.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = WHITE;
					musicLow = DARKGRAY;
					musicHigh = WHITE;
					audio::musicVolume = 0.5f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY4, (float)texture::highButton.width, (float)texture::highButton.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = WHITE;
					musicLow = WHITE;
					musicHigh = DARKGRAY;
					audio::musicVolume = 1.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY6, (float)texture::muteButton.width, (float)texture::muteButton.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = DARKGRAY;
					soundsLow = WHITE;
					soundsHigh = WHITE;
					audio::soundVolume = 0.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY7, (float)texture::lowButton.width, (float)texture::lowButton.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = WHITE;
					soundsLow = DARKGRAY;
					soundsHigh = WHITE;
					audio::soundVolume = 2.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY8, (float)texture::highButton.width, (float)texture::highButton.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = WHITE;
					soundsLow = WHITE;
					soundsHigh = DARKGRAY;
					audio::soundVolume = 4.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::settingsButton2.width, (float)texture::settingsButton2.height }))
			{
				settingsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
				}
			}
			else settingsButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				gameButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::previousScene;
				}
			}
			else
			{
				menuButtonStatus = WHITE;
				gameButtonStatus = WHITE;
			}

			if (game::previousScene == game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
			}
			else
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
			}

			audio::setSoundsVolume();
			audio::setMusicVolume();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Music", (screenWidth / 2) - MeasureText("Music", sizes::text3 / 2), pos::audioPosY, sizes::text3, BLACK);
			DrawTexture(texture::muteButton, pos::middleButtonsPosX, pos::audioPosY2, musicMute);
			DrawTexture(texture::lowButton, pos::middleButtonsPosX, pos::audioPosY3, musicLow);
			DrawTexture(texture::highButton, pos::middleButtonsPosX, pos::audioPosY4, musicHigh);

			DrawText("Sounds", (screenWidth / 2) - MeasureText("Sounds", sizes::text3 / 2), pos::audioPosY5, sizes::text3, WHITE);
			DrawTexture(texture::muteButton, pos::middleButtonsPosX, pos::audioPosY6, soundsMute);
			DrawTexture(texture::lowButton, pos::middleButtonsPosX, pos::audioPosY7, soundsLow);
			DrawTexture(texture::highButton, pos::middleButtonsPosX, pos::audioPosY8, soundsHigh);


			DrawTexture(texture::settingsButton2, pos::buttonsLeft, pos::buttonsUp, settingsButtonStatus);
			if (game::previousScene == game::Scene::GAMEPLAY) DrawTexture(texture::gameButton, pos::buttonsRight, pos::buttonsUp, gameButtonStatus);
			else DrawTexture(texture::menuButton, pos::buttonsRight, pos::buttonsUp, menuButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(texture::muteButton);
			UnloadTexture(texture::lowButton);
			UnloadTexture(texture::highButton);
		}
	}
}