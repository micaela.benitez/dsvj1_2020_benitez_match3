#include "resolutions.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace resolutions
	{
		static Color resolution1ButtonStatus = DARKGRAY;
		static Color resolution2ButtonStatus = WHITE;
		static Color resolution3ButtonStatus = WHITE;

		static Color settingsButtonStatus = WHITE;
		static Color gameButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{
			texture::resolution1Button = LoadTexture("res/raw/textures/resolution1.png");
			texture::resolution1Button.width = sizes::buttonsWidth;
			texture::resolution1Button.height = sizes::buttonsHeight;

			texture::resolution2Button = LoadTexture("res/raw/textures/resolution2.png");
			texture::resolution2Button.width = sizes::buttonsWidth;
			texture::resolution2Button.height = sizes::buttonsHeight;

			texture::resolution3Button = LoadTexture("res/raw/textures/resolution3.png");
			texture::resolution3Button.width = sizes::buttonsWidth;
			texture::resolution3Button.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::resolutionPosY2, (float)texture::resolution1Button.width, (float)texture::resolution1Button.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					resolution1ButtonStatus = DARKGRAY;
					resolution2ButtonStatus = WHITE;
					resolution3ButtonStatus = WHITE;
					screenWidth = 700;
					screenHeight = 1000;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::resolutionPosY3, (float)texture::resolution2Button.width, (float)texture::resolution2Button.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					resolution1ButtonStatus = WHITE;
					resolution2ButtonStatus = DARKGRAY;
					resolution3ButtonStatus = WHITE;
					screenWidth = 600;
					screenHeight = 900;					
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::resolutionPosY4, (float)texture::resolution3Button.width, (float)texture::resolution3Button.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					resolution1ButtonStatus = WHITE;
					resolution2ButtonStatus = WHITE;
					resolution3ButtonStatus = DARKGRAY;
					screenWidth = 500;
					screenHeight = 800;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::settingsButton2.width, (float)texture::settingsButton2.height }))
			{
				settingsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
				}
			}
			else settingsButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				gameButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::previousScene;
				}
			}
			else
			{
				menuButtonStatus = WHITE;
				gameButtonStatus = WHITE;
			}

			if (game::previousScene == game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
			}
			else
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
			}

			changedResolution = true;
			SetWindowSize(screenWidth, screenHeight);
			initialize();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Resolution", (screenWidth / 2) - MeasureText("Resolution", sizes::text3 / 2), pos::resolutionPosY, sizes::text3, BLACK);
			DrawTexture(texture::resolution1Button, pos::middleButtonsPosX, pos::resolutionPosY2, resolution1ButtonStatus);
			DrawTexture(texture::resolution2Button, pos::middleButtonsPosX, pos::resolutionPosY3, resolution2ButtonStatus);
			DrawTexture(texture::resolution3Button, pos::middleButtonsPosX, pos::resolutionPosY4, resolution3ButtonStatus);

			DrawTexture(texture::settingsButton2, pos::buttonsLeft, pos::buttonsUp, settingsButtonStatus);
			if (game::previousScene == game::Scene::GAMEPLAY) DrawTexture(texture::gameButton, pos::buttonsRight, pos::buttonsUp, gameButtonStatus);
			else DrawTexture(texture::menuButton, pos::buttonsRight, pos::buttonsUp, menuButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(texture::resolution1Button);
			UnloadTexture(texture::resolution2Button);
			UnloadTexture(texture::resolution3Button);
		}
	}
}