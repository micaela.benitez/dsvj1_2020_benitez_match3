#include "menu.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace menu
	{
		static Color playButtonStatus = WHITE;
		static Color instructionsButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color creditsButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;

		void init()
		{
			texture::backgroundMenu = LoadTexture("res/assets/textures/backgroundmenu.png");
			texture::backgroundMenu.width = screenWidth;
			texture::backgroundMenu.height = screenHeight;

			texture::playButton = LoadTexture("res/raw/textures/playbutton.png");
			texture::playButton.width = sizes::buttonsWidth;
			texture::playButton.height = sizes::buttonsHeight;

			texture::instructionsButton = LoadTexture("res/raw/textures/instructionsbutton.png");
			texture::instructionsButton.width = sizes::buttonsWidth;
			texture::instructionsButton.height = sizes::buttonsHeight;

			texture::creditsButton = LoadTexture("res/raw/textures/creditsbutton.png");
			texture::creditsButton.width = sizes::buttonsWidth;
			texture::creditsButton.height = sizes::buttonsHeight;

			texture::exitButton = LoadTexture("res/raw/textures/exitbutton.png");
			texture::exitButton.width = sizes::buttonsWidth;
			texture::exitButton.height = sizes::buttonsHeight;

			texture::settingsButton = LoadTexture("res/raw/textures/settingsbutton.png");
			texture::settingsButton.width = sizes::buttonsWidth;
			texture::settingsButton.height = sizes::buttonsHeight;

			// Audio
			audio::loadSounds();
			audio::loadMusic();
			audio::setSoundsVolume();
			audio::setMusicVolume();
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::menuPosY, (float)texture::playButton.width, (float)texture::playButton.height }))
			{
				playButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::GAMEPLAY;
					StopMusicStream(audio::gameMusic.menu);
					initialize();
				}
			}
			else playButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::menuPosY2, (float)texture::instructionsButton.width, (float)texture::instructionsButton.height }))
			{
				instructionsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS;
				}
			}
			else instructionsButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::menuPosY3, (float)texture::settingsButton.width, (float)texture::settingsButton.height }))
			{
				settingsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
					game::previousScene = game::Scene::MENU;
				}
			}
			else settingsButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::menuPosY4, (float)texture::creditsButton.width, (float)texture::creditsButton.height }))
			{
				creditsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS;
				}
			}
			else creditsButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::menuPosY5, (float)texture::exitButton.width, (float)texture::exitButton.height }))
			{
				exitButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::exitButton = true;
				}
			}
			else exitButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Ninja", (screenWidth / 2) - MeasureText("Ninja", sizes::title / 2), pos::titlePoxY, sizes::title, BLACK);
			DrawText("match 3", (screenWidth / 2) - MeasureText("match 3", sizes::title / 2), pos::titlePoxY2, sizes::title, BLACK);

			DrawTexture(texture::playButton, pos::middleButtonsPosX, pos::menuPosY, playButtonStatus);
			DrawTexture(texture::instructionsButton, pos::middleButtonsPosX, pos::menuPosY2, instructionsButtonStatus);
			DrawTexture(texture::settingsButton, pos::middleButtonsPosX, pos::menuPosY3, settingsButtonStatus);
			DrawTexture(texture::creditsButton, pos::middleButtonsPosX, pos::menuPosY4, creditsButtonStatus);
			DrawTexture(texture::exitButton, pos::middleButtonsPosX, pos::menuPosY5, exitButtonStatus);

			DrawText("V 2.0", pos::versionPosX, pos::versionPosY, sizes::text, GRAY);
		}

		void deinit()
		{
			UnloadTexture(texture::backgroundMenu);
			UnloadTexture(texture::playButton);
			UnloadTexture(texture::instructionsButton);
			UnloadTexture(texture::settingsButton);
			UnloadTexture(texture::creditsButton);
			UnloadTexture(texture::exitButton);
		}
	}
}