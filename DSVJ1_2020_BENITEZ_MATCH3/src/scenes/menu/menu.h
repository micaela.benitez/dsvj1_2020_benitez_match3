#ifndef MENU_H
#define MENU_H

#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace menu
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif