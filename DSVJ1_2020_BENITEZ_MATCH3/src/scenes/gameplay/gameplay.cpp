#include "gameplay.h"

namespace match3
{
	namespace gameplay
	{
		int screenWidth = 700;
		int screenHeight = 1000;

		Vector2 mousePoint = { 0.0f, 0.0f };

		int gridSizeAux = 0;
		int gridSize = 0;
	
		Pieces piece[piecesPerLine][piecesPerColumn];
		int totalQuantitySpecialPieces = 1;
		int quantitySpecialPieces = 0;
		Kind specialPiece;
		Texture2D breakable;
		int hitsBreakable = 0;

		Node* pile = NULL;

		float timerCharacterGirl = 0.0f;
		int frameCharacterGirl = 0;;
		float frameWidth = 0.0f;
		int maxFrames = 0;

		float timerCharacterGirlThrow = 0.0f;
		int frameCharacterGirlThrow = 0;
		float frameWidth2 = 0.0f;
		int maxFrames2 = 0;

		float timerCharacterGirlThrow2 = 0.0f;
		int frameCharacterGirlThrow2 = 0;
		float frameWidth3 = 0.0f;
		int maxFrames3 = 0;

		float timerCharacterGirlDead = 0.0f;
		int frameCharacterGirlDead = 0;
		float frameWidth4 = 0.0f;
		int maxFrames4 = 0;

		float timerCharacterBoy = 0.0f;
		int frameCharacterBoy = 0;;
		float frameWidth5 = 0.0f;
		int maxFrames5 = 0;

		float timerCharacterBoyThrow = 0.0f;
		int frameCharacterBoyThrow = 0;;
		float frameWidth6 = 0.0f;
		int maxFrames6 = 0;

		float timerCharacterBoyThrow2 = 0.0f;
		int frameCharacterBoyThrow2 = 0;;
		float frameWidth7 = 0.0f;
		int maxFrames7 = 0;

		float timerCharacterBoyDead = 0.0f;
		int frameCharacterBoyDead = 0;;
		float frameWidth8 = 0.0f;
		int maxFrames8 = 0;

		int cantFrames = 0;

		Characters girl;
		Characters boy;

		float kunaiGirlActualPos = pos::kunaiGirlInitialPos;
		float kunaiBoyActualPos = pos::kunaiBoyInitialPos;

		int score = 0;
		int highScore = 0;

		Vector2 backgroundPieces[piecesPerLine][piecesPerColumn];

		Color settingsButtonStatus = WHITE;
		Color menuButtonStatus = WHITE;

		int quantitySelectedPieces = 0;
		bool piecesMoving = false;
		bool piecesRegenerating = false;
		float speedPieces = 0;

		Sounds sounds;

		bool initialDraw = true;
		bool introduction = true;

		bool pause = false;
		bool changedResolution = false;

		int regerationGrid = maximumRegerationGrid;

		int actualBackground = 0;

		Color screenDetails;

		void init()
		{
			texture::attack = LoadTexture("res/assets/textures/attack.png");
			texture::attack.width = sizes::pieces;
			texture::attack.height = sizes::pieces;

			texture::attack2 = LoadTexture("res/assets/textures/attack2.png");
			texture::attack2.width = sizes::pieces;
			texture::attack2.height = sizes::pieces;

			texture::energy = LoadTexture("res/assets/textures/energy.png");
			texture::energy.width = sizes::pieces;
			texture::energy.height = sizes::pieces;

			texture::coin = LoadTexture("res/assets/textures/coin.png");
			texture::coin.width = sizes::pieces;
			texture::coin.height = sizes::pieces;

			texture::breakable1 = LoadTexture("res/assets/textures/breakable.png");
			texture::breakable1.width = sizes::pieces;
			texture::breakable1.height = sizes::pieces;

			texture::breakable2 = LoadTexture("res/raw/textures/breakable2.png");
			texture::breakable2.width = sizes::pieces;
			texture::breakable2.height = sizes::pieces;

			texture::breakable3 = LoadTexture("res/raw/textures/breakable3.png");
			texture::breakable3.width = sizes::pieces;
			texture::breakable3.height = sizes::pieces;

			texture::wildcard = LoadTexture("res/raw/textures/wildcard.png");
			texture::wildcard.width = sizes::pieces;
			texture::wildcard.height = sizes::pieces;

			texture::colorPickerViolet = LoadTexture("res/assets/textures/violet.png");
			texture::colorPickerViolet.width = sizes::pieces;
			texture::colorPickerViolet.height = sizes::pieces;

			texture::colorPickerGreen = LoadTexture("res/assets/textures/green.png");
			texture::colorPickerGreen.width = sizes::pieces;
			texture::colorPickerGreen.height = sizes::pieces;

			texture::colorPickerRed = LoadTexture("res/assets/textures/red.png");
			texture::colorPickerRed.width = sizes::pieces;
			texture::colorPickerRed.height = sizes::pieces;

			texture::colorPickerYellow = LoadTexture("res/assets/textures/yellow.png");
			texture::colorPickerYellow.width = sizes::pieces;
			texture::colorPickerYellow.height = sizes::pieces;

			texture::bomb = LoadTexture("res/assets/textures/bomb.png");
			texture::bomb.width = sizes::pieces;
			texture::bomb.height = sizes::pieces;

			texture::tntVertical = LoadTexture("res/assets/textures/tntvertical.png");
			texture::tntVertical.width = sizes::pieces;
			texture::tntVertical.height = sizes::pieces;

			texture::tntHorizontal = LoadTexture("res/assets/textures/tnthorizontal.png");
			texture::tntHorizontal.width = sizes::pieces;
			texture::tntHorizontal.height = sizes::pieces;

			texture::background = LoadTexture("res/assets/textures/background.png");
			texture::background.width = screenWidth;
			texture::background.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::background2 = LoadTexture("res/assets/textures/background2.png");
			texture::background2.width = screenWidth;
			texture::background2.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::background3 = LoadTexture("res/assets/textures/background3.png");
			texture::background3.width = screenWidth;
			texture::background3.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::background4 = LoadTexture("res/assets/textures/background4.png");
			texture::background4.width = screenWidth;
			texture::background4.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::background5 = LoadTexture("res/assets/textures/background5.png");
			texture::background5.width = screenWidth;
			texture::background5.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::background6 = LoadTexture("res/assets/textures/background6.png");
			texture::background6.width = screenWidth;
			texture::background6.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::background7 = LoadTexture("res/assets/textures/background7.png");
			texture::background7.width = screenWidth;
			texture::background7.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

			texture::characterGirl = LoadTexture("res/assets/textures/ninja1.png");
			texture::characterGirl.width = sizes::charactersWidth * sizes::charactersFrames;
			texture::characterGirl.height = sizes::charactersHeight;

			texture::characterGirlThrow = LoadTexture("res/assets/textures/ninja1throw.png");
			texture::characterGirlThrow.width = sizes::charactersWidthThrow * sizes::charactersFrames;
			texture::characterGirlThrow.height = sizes::charactersHeight;

			texture::characterGirlThrow2 = LoadTexture("res/assets/textures/ninja1throw2.png");
			texture::characterGirlThrow2.width = sizes::charactersWidthThrow * sizes::charactersFrames;
			texture::characterGirlThrow2.height = sizes::charactersHeight;

			texture::characterGirlDead = LoadTexture("res/assets/textures/ninja1dead.png");
			texture::characterGirlDead.width = sizes::charactersWidthDead * sizes::characterDeadFrames;
			texture::characterGirlDead.height = sizes::charactersHeight;

			texture::characterBoy = LoadTexture("res/assets/textures/ninja2.png");
			texture::characterBoy.width = sizes::charactersWidth * sizes::charactersFrames;
			texture::characterBoy.height = sizes::charactersHeight;
			
			texture::characterBoyThrow = LoadTexture("res/assets/textures/ninja2throw.png");
			texture::characterBoyThrow.width = sizes::charactersWidthThrow * sizes::charactersFrames;
			texture::characterBoyThrow.height = sizes::charactersHeight;

			texture::characterBoyThrow2 = LoadTexture("res/assets/textures/ninja2throw2.png");
			texture::characterBoyThrow2.width = sizes::charactersWidthThrow * sizes::charactersFrames;
			texture::characterBoyThrow2.height = sizes::charactersHeight;

			texture::characterBoyDead = LoadTexture("res/assets/textures/ninja2dead.png");
			texture::characterBoyDead.width = sizes::charactersWidthDead * sizes::characterDeadFrames;
			texture::characterBoyDead.height = sizes::charactersHeight;

			texture::menuButton = LoadTexture("res/raw/textures/menu.png");
			texture::menuButton.width = sizes::circleButtons;
			texture::menuButton.height = sizes::circleButtons;

			texture::gameButton = LoadTexture("res/raw/textures/game.png");
			texture::gameButton.width = sizes::circleButtons;
			texture::gameButton.height = sizes::circleButtons;

			texture::settingsButton2 = LoadTexture("res/raw/textures/settings.png");
			texture::settingsButton2.width = sizes::circleButtons;
			texture::settingsButton2.height = sizes::circleButtons;

			texture::rightArrowButton = LoadTexture("res/raw/textures/rightarrow.png");
			texture::rightArrowButton.width = sizes::circleButtons;
			texture::rightArrowButton.height = sizes::circleButtons;

			texture::leftArrowButton = LoadTexture("res/raw/textures/leftarrow.png");
			texture::leftArrowButton.width = sizes::circleButtons;
			texture::leftArrowButton.height = sizes::circleButtons;

			texture::kunaiGirl = LoadTexture("res/assets/textures/kunaigirl.png");
			texture::kunaiGirl.width = sizes::kunaiWidth;
			texture::kunaiGirl.height = sizes::kunaiHeight;

			texture::kunaiBoy = LoadTexture("res/assets/textures/kunaiboy.png");
			texture::kunaiBoy.width = sizes::kunaiWidth;
			texture::kunaiBoy.height = sizes::kunaiHeight;

			initPieces();

			gridSizeAux = screenWidth / piecesPerColumn;
			gridSize = (screenWidth - (gridSizeAux - sizes::pieces)) / piecesPerColumn;

			girl.live = sizes::recWidth1;
			girl.shield = sizes::recWidth2;
			girl.attack = 0;
			girl.attack2 = 0;
			girl.attackStatus = false;
			girl.attack2Status = false;
			girl.deadStatus = false;
			girl.kunai = false;
			boy.live = sizes::recWidth1;
			boy.shield = sizes::recWidth2;
			boy.attack = 0;
			boy.attack2 = 0;
			boy.attackStatus = false;
			boy.attack2Status = false;
			boy.deadStatus = false;
			boy.kunai = false;
		}

		void initialize()
		{
			gridSizeAux = screenWidth / piecesPerColumn;
			gridSize = (screenWidth - (gridSizeAux - sizes::pieces)) / piecesPerColumn;

			breakable = texture::breakable1;
			hitsBreakable = 0;
			speedPieces = 0.0f;
			initPieces();

			if (!changedResolution)
			{
				piecesMoving = false;
				piecesRegenerating = false;
				initialDraw = true;
				pause = false;
				introduction = true;
				regerationGrid = maximumRegerationGrid;
				girl.live = sizes::recWidth1;
				girl.shield = sizes::recWidth2;
				girl.attack = 0;
				girl.attack2 = 0;
				girl.attackStatus = false;
				girl.attack2Status = false;
				girl.deadStatus = false;
				girl.kunai = false;
				boy.live = sizes::recWidth1;
				boy.shield = sizes::recWidth2;
				boy.attack = 0;
				boy.attack2 = 0;
				boy.attackStatus = false;
				boy.attack2Status = false;
				boy.deadStatus = false;
				boy.kunai = false;
				kunaiGirlActualPos = pos::kunaiGirlInitialPos;
				kunaiBoyActualPos = pos::kunaiBoyInitialPos;

				actualBackground = GetRandomValue(1,7);

				if (actualBackground == 1) screenDetails = BLACK;
				else if (actualBackground == 2) screenDetails = WHITE;
				else if (actualBackground == 3) screenDetails = WHITE;
				else if (actualBackground == 4) screenDetails = WHITE;
				else if (actualBackground == 5) screenDetails = BLACK;
				else if (actualBackground == 6) screenDetails = WHITE;
				else if (actualBackground == 7) screenDetails = WHITE;
			}
			else
			{
				initialDraw = false;
				pause = false;
				kunaiGirlActualPos = pos::kunaiGirlInitialPos;
				kunaiBoyActualPos = pos::kunaiBoyInitialPos;
			}

			changedResolution = (changedResolution == true) ? false : false;
		}

		void update()
		{
			if (IsKeyPressed(KEY_SPACE)) pause = (pause == false) ? true : false;   // Pause
			if (IsKeyPressed(KEY_ENTER) && !pause)   // Regenerate pieces
			{
				if (regerationGrid > 0)
				{
					regerationGrid--;
					speedPieces = 0.0f;
					initPieces();
					initialDraw = true;
				}
			}

			if (!pause)
			{
				frames();
				selectionPieces();
				deselectionPieces();
				removePieces();
				movePieces();
				generateNewPieces();
				updateBars();
				fightCharacters();
			}
			else updateScreenDetails();

			if (score > highScore) highScore = score;   // Highscore

			if (girl.live <= 0.0f || boy.live <= 0.0f)   // Result
			{
				game::currentScene = game::Scene::RESULT;
				StopMusicStream(audio::gameMusic.gameplay);
			}

			PlayMusicStream(audio::gameMusic.gameplay);
			audio::gameplayAudio();

			sounds.button = false;
			sounds.pieceSelected = false;
			sounds.pieceDeselected = false;
			sounds.fallenCharacter = false;
			sounds.fallenPiece = false;
			sounds.piecesDisappear = false;
			sounds.bomb = false;
			sounds.wrongSelection = false;
		}

		void draw()
		{
			drawPieces();
			drawBackground();
			drawCharacters();
			drawScreenDetails();
		}

		void deinit()
		{
			UnloadTexture(texture::attack);
			UnloadTexture(texture::attack2);
			UnloadTexture(texture::energy);
			UnloadTexture(texture::coin);
			UnloadTexture(texture::breakable1);
			UnloadTexture(texture::breakable2);
			UnloadTexture(texture::breakable3);
			UnloadTexture(texture::wildcard);
			UnloadTexture(texture::colorPickerViolet);
			UnloadTexture(texture::colorPickerGreen);
			UnloadTexture(texture::colorPickerRed);
			UnloadTexture(texture::colorPickerYellow);
			UnloadTexture(texture::bomb);
			UnloadTexture(texture::tntVertical);
			UnloadTexture(texture::tntHorizontal);
			UnloadTexture(texture::background);
			UnloadTexture(texture::background2);
			UnloadTexture(texture::background3);
			UnloadTexture(texture::background4);
			UnloadTexture(texture::background5);
			UnloadTexture(texture::background6);
			UnloadTexture(texture::background7);
			UnloadTexture(texture::characterGirl);
			UnloadTexture(texture::characterGirlThrow);
			UnloadTexture(texture::characterGirlThrow2);
			UnloadTexture(texture::characterGirlDead);
			UnloadTexture(texture::characterBoy);
			UnloadTexture(texture::characterBoyThrow);
			UnloadTexture(texture::characterBoyThrow2);
			UnloadTexture(texture::characterBoyDead);
			UnloadTexture(texture::menuButton);
			UnloadTexture(texture::gameButton);
			UnloadTexture(texture::settingsButton2);
			UnloadTexture(texture::rightArrowButton);
			UnloadTexture(texture::leftArrowButton);
			UnloadTexture(texture::kunaiGirl);
			UnloadTexture(texture::kunaiBoy);
		}
	}
}