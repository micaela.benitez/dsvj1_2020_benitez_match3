#ifndef GAMEPLAY_H
#define GAMEPLAY_H

//#include <cstdlib>

#include "raylib.h"

#include "functions_library/textures/textures.h"
#include "functions_library/audio/sounds.h"
#include "functions_library/audio/audio.h"
#include "functions_library/entities/entities.h"
#include "functions_library/entities_motion/entities_motion.h"
#include "functions_library/drawings/drawings.h"
#include "functions_library/sizes/sizes.h"
#include "functions_library/positions/positions.h"
#include "functions_library/points/points.h"
#include "functions_library/selection/node.h"
#include "scenes/game/game.h"

const int piecesPerLine = 6;
const int piecesPerColumn = 8;
const int minimumSelectedPieces = 3;
const int maximumRegerationGrid = 3;

namespace match3
{
	namespace gameplay
	{
		extern int screenWidth;
		extern int screenHeight;

		extern Vector2 mousePoint;

		extern int gridSizeAux;
		extern int gridSize;

		extern Pieces piece[piecesPerLine][piecesPerColumn];
		extern int totalQuantitySpecialPieces;
		extern int quantitySpecialPieces;
		extern Kind specialPiece;
		extern Texture2D breakable;
		extern int hitsBreakable;

		extern Node* pile;

		extern float timerCharacterGirl;
		extern int frameCharacterGirl;
		extern float frameWidth;
		extern int maxFrames;

		extern float timerCharacterGirlThrow;
		extern int frameCharacterGirlThrow;
		extern float frameWidth2;
		extern int maxFrames2;

		extern float timerCharacterGirlThrow2;
		extern int frameCharacterGirlThrow2;
		extern float frameWidth3;
		extern int maxFrames3;

		extern float timerCharacterGirlDead;
		extern int frameCharacterGirlDead;
		extern float frameWidth4;
		extern int maxFrames4;

		extern float timerCharacterBoy;
		extern int frameCharacterBoy;
		extern float frameWidth5;
		extern int maxFrames5;

		extern float timerCharacterBoyThrow;
		extern int frameCharacterBoyThrow;
		extern float frameWidth6;
		extern int maxFrames6;

		extern float timerCharacterBoyThrow2;
		extern int frameCharacterBoyThrow2;
		extern float frameWidth7;
		extern int maxFrames7;

		extern float timerCharacterBoyDead;
		extern int frameCharacterBoyDead;
		extern float frameWidth8;
		extern int maxFrames8;

		extern int cantFrames;

		extern Characters girl;
		extern Characters boy;

		extern float kunaiGirlActualPos;
		extern float kunaiBoyActualPos;

		extern int score;
		extern int highScore;

		extern Vector2 backgroundPieces[piecesPerLine][piecesPerColumn];

		extern Color settingsButtonStatus;
		extern Color menuButtonStatus;

		extern int quantitySelectedPieces;
		extern bool piecesMoving;
		extern bool piecesRegenerating;
		extern float speedPieces;

		extern Sounds sounds;

		extern bool initialDraw;
		extern bool introduction;

		extern bool pause;
		extern bool changedResolution;

		extern int regerationGrid;

		extern int actualBackground;

		extern Color screenDetails;

		void init();
		void initialize();
		void update();
		void draw();
		void deinit();
	}
}

#endif