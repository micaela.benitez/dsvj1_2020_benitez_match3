#include "game.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace game
	{
		Scene currentScene;
		Scene previousScene;

		bool exitButton;

		Sounds sounds;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, "MATCH 3");
			InitAudioDevice();
			SetTargetFPS(60);

			currentScene = Scene::MENU;

			menu::init();
			instructions::init();
			instructions2::init();
			instructions3::init();
			settings::init();
			credits::init();
			credits2::init();
			gameplay::init();
			result::init();
			game_audio::init();
			resolutions::init();
		}

		static void update()
		{
			pos::updateVariables();
			sizes::updateSizes();
			texture::updateTextures();

			switch (currentScene)
			{
			case Scene::MENU:
				menu::update();
				break;
			case Scene::INSTRUCTIONS:
				instructions::update();
				break;
			case Scene::INSTRUCTIONS2:
				instructions2::update();
				break;
			case Scene::INSTRUCTIONS3:
				instructions3::update();
				break;
			case Scene::SETTINGS:
				settings::update();
				break;
			case Scene::CREDITS:
				credits::update();
				break;
			case Scene::CREDITS2:
				credits2::update();
				break;
			case Scene::GAMEPLAY:
				gameplay::update();
				break;
			case Scene::RESULT:
				result::update();
				break;
			case Scene::AUDIO:
				game_audio::update();
				break;
			case Scene::RESOLUTIONS:
				resolutions::update();
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);

			switch (currentScene)
			{
			case Scene::MENU:
				menu::draw();
				break;
			case Scene::INSTRUCTIONS:
				instructions::draw();
				break;
			case Scene::INSTRUCTIONS2:
				instructions2::draw();
				break;
			case Scene::INSTRUCTIONS3:
				instructions3::draw();
				break;
			case Scene::SETTINGS:
				settings::draw();
				break;
			case Scene::CREDITS:
				credits::draw();
				break;
			case Scene::CREDITS2:
				credits2::draw();
				break;
			case Scene::GAMEPLAY:
				gameplay::draw();
				break;
			case Scene::RESULT:
				result::draw();
				break;
			case Scene::AUDIO:
				game_audio::draw();
				break;
			case Scene::RESOLUTIONS:
				resolutions::draw();
				break;
			}

			EndDrawing();
		}

		static void deinit()
		{
			CloseWindow();

			menu::deinit();
			instructions::deinit();
			instructions2::deinit();
			instructions3::deinit();
			settings::deinit();
			credits::deinit();
			credits2::deinit();
			gameplay::deinit();
			result::deinit();
			game_audio::deinit();
			resolutions::deinit();
		}

		void run()
		{
			init();

			while (!WindowShouldClose() && !exitButton)
			{
				update();
				draw();
			}

			deinit();
		}
	}
}