#ifndef INSTRUCTIONS3_H
#define INSTRUCTIONS3_H

#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace instructions3
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif