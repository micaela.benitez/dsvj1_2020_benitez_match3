#include "instructions.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace instructions
	{
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::rightArrowButton.width, (float)texture::rightArrowButton.height }))
			{
				rightArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS2;
				}
			}
			else rightArrowButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("In this game you will have to make", (screenWidth / 2) - MeasureText("In this game you will have to make", sizes::text / 2), pos::instructionsPosY, sizes::text, BLACK);
			DrawText("combinations of three or more equal ", (screenWidth / 2) - MeasureText("combinations of three or more equal ", sizes::text / 2), pos::instructionsPosY2, sizes::text, BLACK);
			DrawText("pieces to fill the bars of your player", (screenWidth / 2) - MeasureText("pieces to fill the bars of your player", sizes::text / 2), pos::instructionsPosY3, sizes::text, BLACK);
			DrawText("and win the fight! (Except for the ", (screenWidth / 2) - MeasureText("and win the fight! (Except for the ", sizes::text / 2), pos::instructionsPosY4, sizes::text, BLACK);
			DrawText("special tiles, no need to make ", (screenWidth / 2) - MeasureText("special tiles, no need to make ", sizes::text / 2), pos::instructionsPosY5, sizes::text, BLACK);
			DrawText("combinations with them)", (screenWidth / 2) - MeasureText("combinations with them)", sizes::text / 2), pos::instructionsPosY6, sizes::text, BLACK);

			DrawText("The more fights won, the more points", (screenWidth / 2) - MeasureText("The more fights won, the more points", sizes::text / 2), pos::instructionsPosY7, sizes::text, WHITE);
			DrawText("you will get, but if you lose a fight,", (screenWidth / 2) - MeasureText("you will get, but if you lose a fight,", sizes::text / 2), pos::instructionsPosY8, sizes::text, WHITE);
			DrawText("you lose all your accumulated points.", (screenWidth / 2) - MeasureText("you lose all your accumulated points.", sizes::text / 2), pos::instructionsPosY9, sizes::text, WHITE);

			DrawText("Each piece fills the bar with its", (screenWidth / 2) - MeasureText("Each piece fills the bar with its", sizes::text / 2), pos::instructionsPosY10, sizes::text, WHITE);
			DrawText("respective color. There are pieces ", (screenWidth / 2) - MeasureText("respective color. There are pieces ", sizes::text / 2), pos::instructionsPosY11, sizes::text, WHITE);
			DrawText("for life, for points and others", (screenWidth / 2) - MeasureText("for life, for points and others", sizes::text / 2), pos::instructionsPosY12, sizes::text, WHITE);
			DrawText("to attack your opponent!", (screenWidth / 2) - MeasureText("to attack your opponent!", sizes::text / 2), pos::instructionsPosY13, sizes::text, WHITE);

			DrawTexture(texture::menuButton, pos::buttonsLeft, pos::buttonsUp, menuButtonStatus);
			DrawTexture(texture::rightArrowButton, pos::buttonsRight, pos::buttonsUp, rightArrowButtonStatus);
		}


		void deinit()
		{

		}
	}
}