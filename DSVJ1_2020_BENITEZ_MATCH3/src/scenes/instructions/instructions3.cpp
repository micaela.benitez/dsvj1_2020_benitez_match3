#include "instructions3.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace instructions3
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::leftArrowButton.width, (float)texture::leftArrowButton.height }))
			{
				leftArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS2;
				}
			}
			else leftArrowButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Special pieces", (screenWidth / 2) - MeasureText("Special pieces", sizes::text2 / 2), pos::instructions3PosY, sizes::text2, BLACK);

			DrawTexture(texture::bomb, pos::instructions3PosX, pos::instructions3PosY2, WHITE);
			DrawText("Break all", pos::instructions3PosX2, pos::instructions3PosY3, sizes::text, BLACK);
			DrawText("the pieces", pos::instructions3PosX3, pos::instructions3PosY4, sizes::text, BLACK);
			DrawText("around", pos::instructions3PosX4, pos::instructions3PosY5, sizes::text, BLACK);
			DrawTexture(texture::breakable1, (screenWidth / 2) - (texture::breakable1.width / 2), pos::instructions3PosY2, WHITE);
			DrawText("It breaks when", (screenWidth / 2) - MeasureText("It breaks when", sizes::text / 2), pos::instructions3PosY3, sizes::text, BLACK);
			DrawText("the pieces", (screenWidth / 2) - MeasureText("the pieces", sizes::text / 2), pos::instructions3PosY4, sizes::text, BLACK);
			DrawText("around it break", (screenWidth / 2) - MeasureText("around it break", sizes::text / 2), pos::instructions3PosY5, sizes::text, BLACK);
			DrawTexture(texture::wildcard, pos::instructions3PosX5, pos::instructions3PosY2, WHITE);
			DrawText("You can", pos::instructions3PosX6, pos::instructions3PosY3, sizes::text, BLACK);
			DrawText("use it like", pos::instructions3PosX7, pos::instructions3PosY4, sizes::text, BLACK);
			DrawText("any piece", pos::instructions3PosX8, pos::instructions3PosY5, sizes::text, BLACK);

			DrawTexture(texture::colorPickerViolet, pos::instructions3PosX9, pos::instructions3PosY6, WHITE);
			DrawTexture(texture::colorPickerGreen, pos::instructions3PosX10, pos::instructions3PosY6, WHITE);
			DrawTexture(texture::colorPickerRed, pos::instructions3PosX11, pos::instructions3PosY6, WHITE);
			DrawTexture(texture::colorPickerYellow, pos::instructions3PosX12, pos::instructions3PosY6, WHITE);
			DrawText("Break all the pieces of their", (screenWidth / 2) - MeasureText("Break all the pieces of their", sizes::text / 2), pos::instructions3PosY7, sizes::text, WHITE);
			DrawText("corresponding color", (screenWidth / 2) - MeasureText("corresponding color", sizes::text / 2), pos::instructions3PosY8, sizes::text, WHITE);

			DrawTexture(texture::tntVertical, pos::instructions3PosX13, pos::instructions3PosY9, WHITE);
			DrawText("Break all the", pos::instructions3PosX14, pos::instructions3PosY10, sizes::text, WHITE);
			DrawText("vertical line", pos::instructions3PosX15, pos::instructions3PosY11, sizes::text, WHITE);
			DrawTexture(texture::tntHorizontal, pos::instructions3PosX16, pos::instructions3PosY9, WHITE);
			DrawText("Break all the", pos::instructions3PosX17, pos::instructions3PosY10, sizes::text, WHITE);
			DrawText("horizontal line", pos::instructions3PosX18, pos::instructions3PosY11, sizes::text, WHITE);

			DrawTexture(texture::leftArrowButton, pos::buttonsLeft, pos::buttonsUp, leftArrowButtonStatus);
			DrawTexture(texture::menuButton, pos::buttonsRight, pos::buttonsUp, menuButtonStatus);
		}


		void deinit()
		{

		}
	}
}