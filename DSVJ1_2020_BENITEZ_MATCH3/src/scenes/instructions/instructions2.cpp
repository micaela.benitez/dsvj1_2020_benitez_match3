#include "instructions2.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace instructions2
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::leftArrowButton.width, (float)texture::leftArrowButton.height }))
			{
				leftArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS;
				}
			}
			else leftArrowButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::rightArrowButton.width, (float)texture::rightArrowButton.height }))
			{
				rightArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS3;
				}
			}
			else rightArrowButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			DrawText("Normal pieces", (screenWidth / 2) - MeasureText("Normal pieces", sizes::text2 / 2), pos::instructions2PosY, sizes::text2, BLACK);

			DrawTexture(texture::coin, (screenWidth / 2) - (texture::coin.width / 2), pos::instructions2PosY2, WHITE);
			DrawText("Collect points", (screenWidth / 2) - MeasureText("Collect points", sizes::text / 2), pos::instructions2PosY3, sizes::text, BLACK);

			DrawTexture(texture::energy, (screenWidth / 2) - (texture::energy.width / 2), pos::instructions2PosY4, WHITE);
			DrawText("Fill the life and shield bar", (screenWidth / 2) - MeasureText("Fill the life and shield bar", sizes::text / 2), pos::instructions2PosY5, sizes::text, BLACK);
			DrawRectangle((screenWidth / 2) - (sizes::recWidth1 / 2), pos::instructions2PosY6, sizes::recWidth1, sizes::recHeight, GREEN);
			DrawRectangle((screenWidth / 2) - (sizes::recWidth2 / 2), pos::instructions2PosY7, sizes::recWidth2, sizes::recHeight, SKYBLUE);
			DrawRectangleLinesEx({ (screenWidth / 2) - (sizes::recWidth1 / 2), pos::instructions2PosY6, sizes::recWidth1, sizes::recHeight }, sizes::recLines, BLACK);
			DrawRectangleLinesEx({ (screenWidth / 2) - (sizes::recWidth2 / 2), pos::instructions2PosY7, sizes::recWidth2, sizes::recHeight }, sizes::recLines, BLACK);

			DrawTexture(texture::attack, (screenWidth / 2) - (texture::attack.width / 2), pos::instructions2PosY8, WHITE);
			DrawText("Fill the attack bar", (screenWidth / 2) - MeasureText("Fill the attack bar", sizes::text / 2), pos::instructions2PosY9, sizes::text, WHITE);
			DrawRectangle((screenWidth / 2) - (sizes::recWidth3 / 2), pos::instructions2PosY10, sizes::recWidth3, sizes::recHeight, RED);
			DrawRectangleLinesEx({ (screenWidth / 2) - (sizes::recWidth3 / 2), pos::instructions2PosY10, sizes::recWidth3, sizes::recHeight }, sizes::recLines, BLACK);

			DrawTexture(texture::attack2, (screenWidth / 2) - (texture::attack2.width / 2), pos::instructions2PosY11, WHITE);
			DrawText("Fill the special attack bar", (screenWidth / 2) - MeasureText("Fill the special attack bar", sizes::text / 2), pos::instructions2PosY12, sizes::text, WHITE);
			DrawRectangle((screenWidth / 2) - (sizes::recWidth3 / 2), pos::instructions2PosY13, sizes::recWidth3, sizes::recHeight, VIOLET);
			DrawRectangleLinesEx({ (screenWidth / 2) - (sizes::recWidth3 / 2), pos::instructions2PosY13, sizes::recWidth3, sizes::recHeight }, sizes::recLines, BLACK);

			DrawTexture(texture::leftArrowButton, pos::buttonsLeft, pos::buttonsUp, leftArrowButtonStatus);
			DrawTexture(texture::rightArrowButton, pos::buttonsRight, pos::buttonsUp, rightArrowButtonStatus);
		}


		void deinit()
		{

		}
	}
}