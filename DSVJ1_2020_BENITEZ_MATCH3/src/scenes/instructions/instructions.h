#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include "scenes/gameplay/gameplay.h"

namespace match3
{
	namespace instructions
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif