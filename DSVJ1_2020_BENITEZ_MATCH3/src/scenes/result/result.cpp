#include "result.h"

using namespace match3;
using namespace gameplay;

namespace match3
{
	namespace result
	{
		static Color playAgainButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{
			texture::playAgainButton = LoadTexture("res/raw/textures/playagainbutton.png");
			texture::playAgainButton.width = sizes::buttonsWidth;
			texture::playAgainButton.height = sizes::buttonsHeight;

			texture::menuButton2 = LoadTexture("res/raw/textures/menubutton.png");
			texture::menuButton2.width = sizes::buttonsWidth;
			texture::menuButton2.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::resultPosX, pos::resultPosY2, (float)texture::playAgainButton.width, (float)texture::playAgainButton.height }))
			{
				playAgainButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::GAMEPLAY;
					initialize();
				}
			}
			else playAgainButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::resultPosX2, pos::resultPosY3, (float)texture::menuButton2.width, (float)texture::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();

			sounds.button = false;
		}

		void draw()
		{
			drawBackground();

			if (girl.live <= 0.0f)
			{
				score = 0;
				DrawText("YOU LOST", (screenWidth / 2) - MeasureText("YOU LOST", sizes::title / 2), pos::resultPosY, sizes::title, BLACK);
			}
			else if (boy.live <= 0.0f) DrawText("YOU WIN", (screenWidth / 2) - MeasureText("YOU WIN", sizes::title / 2), pos::resultPosY, sizes::title, BLACK);

			DrawTexture(texture::playAgainButton, pos::resultPosX, pos::resultPosY2, playAgainButtonStatus);
			DrawTexture(texture::menuButton2, pos::resultPosX2, pos::resultPosY3, menuButtonStatus);

			DrawText(TextFormat("Score: %06i", score), (screenWidth / 2) - MeasureText("Score: 000000", sizes::text2 / 2), pos::resultPosY4, sizes::text2, WHITE);
			DrawText(TextFormat("Highscore: %06i", highScore), (screenWidth / 2) - MeasureText("Highscore: 000000", sizes::text2 / 2), pos::resultPosY5, sizes::text2, WHITE);
		}

		void deinit()
		{
			UnloadTexture(texture::playAgainButton);
			UnloadTexture(texture::menuButton2);
		}
	}
}