#include "textures.h"

using namespace match3;
using namespace gameplay;

namespace texture
{
	Texture2D attack;
	Texture2D attack2;
	Texture2D energy;
	Texture2D coin;
	Texture2D breakable1;
	Texture2D breakable2;
	Texture2D breakable3;
	Texture2D wildcard;
	Texture2D colorPickerViolet;
	Texture2D colorPickerGreen;
	Texture2D colorPickerRed;
	Texture2D colorPickerYellow;
	Texture2D bomb;
	Texture2D tntVertical;
	Texture2D tntHorizontal;
	Texture2D background;
	Texture2D background2;
	Texture2D background3;
	Texture2D background4;
	Texture2D background5;
	Texture2D background6;
	Texture2D background7;
	Texture2D backgroundMenu;
	Texture2D characterGirl;
	Texture2D characterGirlThrow;
	Texture2D characterGirlThrow2;
	Texture2D characterGirlDead;
	Texture2D characterBoy;
	Texture2D characterBoyThrow;
	Texture2D characterBoyThrow2;
	Texture2D characterBoyDead;
	Texture2D kunaiGirl;
	Texture2D kunaiBoy;

	// Button
	Texture2D playButton;
	Texture2D instructionsButton;
	Texture2D settingsButton;
	Texture2D settingsButton2;
	Texture2D creditsButton;
	Texture2D exitButton;
	Texture2D menuButton;
	Texture2D gameButton;
	Texture2D rightArrowButton;
	Texture2D leftArrowButton;
	Texture2D resolutionButton;
	Texture2D audioButton;
	Texture2D playAgainButton;
	Texture2D menuButton2;

	Texture2D muteButton;
	Texture2D lowButton;
	Texture2D highButton;
	Texture2D resolution1Button;
	Texture2D resolution2Button;
	Texture2D resolution3Button;
	 
	void updateTextures()
	{
		texture::attack.width = sizes::pieces;
		texture::attack.height = sizes::pieces;

		texture::attack2.width = sizes::pieces;
		texture::attack2.height = sizes::pieces;

		texture::energy.width = sizes::pieces;
		texture::energy.height = sizes::pieces;

		texture::coin.width = sizes::pieces;
		texture::coin.height = sizes::pieces;

		texture::breakable1.width = sizes::pieces;
		texture::breakable1.height = sizes::pieces;

		texture::breakable2.width = sizes::pieces;
		texture::breakable2.height = sizes::pieces;

		texture::breakable3.width = sizes::pieces;
		texture::breakable3.height = sizes::pieces;

		texture::wildcard.width = sizes::pieces;
		texture::wildcard.height = sizes::pieces;

		texture::colorPickerViolet.width = sizes::pieces;
		texture::colorPickerViolet.height = sizes::pieces;

		texture::colorPickerGreen.width = sizes::pieces;
		texture::colorPickerGreen.height = sizes::pieces;

		texture::colorPickerRed.width = sizes::pieces;
		texture::colorPickerRed.height = sizes::pieces;

		texture::colorPickerYellow.width = sizes::pieces;
		texture::colorPickerYellow.height = sizes::pieces;

		texture::bomb.width = sizes::pieces;
		texture::bomb.height = sizes::pieces;

		texture::tntVertical.width = sizes::pieces;
		texture::tntVertical.height = sizes::pieces;

		texture::tntHorizontal.width = sizes::pieces;
		texture::tntHorizontal.height = sizes::pieces;

		texture::background.width = screenWidth;
		texture::background.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::background2.width = screenWidth;
		texture::background2.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::background3.width = screenWidth;
		texture::background3.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::background4.width = screenWidth;
		texture::background4.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::background5.width = screenWidth;
		texture::background5.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::background6.width = screenWidth;
		texture::background6.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::background7.width = screenWidth;
		texture::background7.height = screenHeight - sizes::pieces * piecesPerLine - gridSize - sizes::pieces / 3;

		texture::characterGirl.width = sizes::charactersWidth * sizes::charactersFrames;
		texture::characterGirl.height = sizes::charactersHeight;

		texture::characterGirlThrow.width = sizes::charactersWidthThrow * sizes::charactersFrames;
		texture::characterGirlThrow.height = sizes::charactersHeight;

		texture::characterGirlThrow2.width = sizes::charactersWidthThrow * sizes::charactersFrames;
		texture::characterGirlThrow2.height = sizes::charactersHeight;

		texture::characterGirlDead.width = sizes::charactersWidthDead * sizes::characterDeadFrames;
		texture::characterGirlDead.height = sizes::charactersHeight;

		texture::characterBoy.width = sizes::charactersWidth * sizes::charactersFrames;
		texture::characterBoy.height = sizes::charactersHeight;

		texture::characterBoyThrow.width = sizes::charactersWidthThrow * sizes::charactersFrames;
		texture::characterBoyThrow.height = sizes::charactersHeight;

		texture::characterBoyThrow2.width = sizes::charactersWidthThrow * sizes::charactersFrames;
		texture::characterBoyThrow2.height = sizes::charactersHeight;

		texture::characterBoyDead.width = sizes::charactersWidthDead * sizes::characterDeadFrames;
		texture::characterBoyDead.height = sizes::charactersHeight;

		texture::menuButton.width = sizes::circleButtons;
		texture::menuButton.height = sizes::circleButtons;

		texture::gameButton.width = sizes::circleButtons;
		texture::gameButton.height = sizes::circleButtons;

		texture::settingsButton2.width = sizes::circleButtons;
		texture::settingsButton2.height = sizes::circleButtons;

		texture::rightArrowButton.width = sizes::circleButtons;
		texture::rightArrowButton.height = sizes::circleButtons;

		texture::leftArrowButton.width = sizes::circleButtons;
		texture::leftArrowButton.height = sizes::circleButtons;

		texture::backgroundMenu.width = screenWidth;
		texture::backgroundMenu.height = screenHeight;

		texture::playButton.width = sizes::buttonsWidth;
		texture::playButton.height = sizes::buttonsHeight;

		texture::instructionsButton.width = sizes::buttonsWidth;
		texture::instructionsButton.height = sizes::buttonsHeight;

		texture::creditsButton.width = sizes::buttonsWidth;
		texture::creditsButton.height = sizes::buttonsHeight;

		texture::exitButton.width = sizes::buttonsWidth;
		texture::exitButton.height = sizes::buttonsHeight;

		texture::settingsButton.width = sizes::buttonsWidth;
		texture::settingsButton.height = sizes::buttonsHeight;

		texture::audioButton.width = sizes::buttonsWidth;
		texture::audioButton.height = sizes::buttonsHeight;

		texture::resolutionButton.width = sizes::buttonsWidth;
		texture::resolutionButton.height = sizes::buttonsHeight;

		texture::resolution1Button.width = sizes::buttonsWidth;
		texture::resolution1Button.height = sizes::buttonsHeight;

		texture::resolution2Button.width = sizes::buttonsWidth;
		texture::resolution2Button.height = sizes::buttonsHeight;

		texture::resolution3Button.width = sizes::buttonsWidth;
		texture::resolution3Button.height = sizes::buttonsHeight;

		texture::muteButton.width = sizes::buttonsWidth;
		texture::muteButton.height = sizes::buttonsHeight;

		texture::lowButton.width = sizes::buttonsWidth;
		texture::lowButton.height = sizes::buttonsHeight;

		texture::highButton.width = sizes::buttonsWidth;
		texture::highButton.height = sizes::buttonsHeight;

		texture::playAgainButton.width = sizes::buttonsWidth;
		texture::playAgainButton.height = sizes::buttonsHeight;

		texture::menuButton2.width = sizes::buttonsWidth;
		texture::menuButton2.height = sizes::buttonsHeight;

		texture::kunaiGirl.width = sizes::kunaiWidth;
		texture::kunaiGirl.height = sizes::kunaiHeight;

		texture::kunaiBoy.width = sizes::kunaiWidth;
		texture::kunaiBoy.height = sizes::kunaiHeight;
	}
}