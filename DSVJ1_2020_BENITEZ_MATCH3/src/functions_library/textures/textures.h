#ifndef TEXTURES_H
#define TEXTURES_H

#include "scenes/gameplay/gameplay.h"

namespace texture
{
	extern Texture2D attack;
	extern Texture2D attack2;
	extern Texture2D energy;
	extern Texture2D coin;
	extern Texture2D breakable1;
	extern Texture2D breakable2;
	extern Texture2D breakable3;
	extern Texture2D wildcard;
	extern Texture2D colorPickerViolet;
	extern Texture2D colorPickerGreen;
	extern Texture2D colorPickerRed;
	extern Texture2D colorPickerYellow;
	extern Texture2D bomb;
	extern Texture2D tntVertical;
	extern Texture2D tntHorizontal;
	extern Texture2D background;
	extern Texture2D background2;
	extern Texture2D background3;
	extern Texture2D background4;
	extern Texture2D background5;
	extern Texture2D background6;
	extern Texture2D background7;
	extern Texture2D backgroundMenu;
	extern Texture2D characterGirl;
	extern Texture2D characterGirlThrow;
	extern Texture2D characterGirlThrow2;
	extern Texture2D characterGirlDead;
	extern Texture2D characterBoy;
	extern Texture2D characterBoyThrow;
	extern Texture2D characterBoyThrow2;
	extern Texture2D characterBoyDead;
	extern Texture2D kunaiGirl;
	extern Texture2D kunaiBoy;

	// Buttons
	extern Texture2D playButton;
	extern Texture2D instructionsButton;
	extern Texture2D settingsButton;
	extern Texture2D settingsButton2;
	extern Texture2D creditsButton;
	extern Texture2D exitButton;
	extern Texture2D menuButton;
	extern Texture2D gameButton;
	extern Texture2D rightArrowButton;
	extern Texture2D leftArrowButton;
	extern Texture2D resolutionButton;
	extern Texture2D audioButton;
	extern Texture2D playAgainButton;
	extern Texture2D menuButton2;

	extern Texture2D muteButton;
	extern Texture2D lowButton;
	extern Texture2D highButton;
	extern Texture2D resolution1Button;
	extern Texture2D resolution2Button;
	extern Texture2D resolution3Button;

	void updateTextures();
}

#endif