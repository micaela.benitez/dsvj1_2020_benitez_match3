#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "scenes/gameplay/gameplay.h"

void drawPieces();
void drawBackground();
void drawCharacters();
void drawScreenDetails();

#endif