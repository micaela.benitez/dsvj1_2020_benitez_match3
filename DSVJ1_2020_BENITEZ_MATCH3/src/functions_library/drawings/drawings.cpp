#include "drawings.h"

using namespace match3;
using namespace gameplay;

void drawPieces()
{
	float speedLerp = 18.25f * GetFrameTime();
	static const float speedLerp2 = 206.0f * GetFrameTime();
	static const float speedLerp3 = 215.0f * GetFrameTime();
	static const float speedLerp4 = 210.0f * GetFrameTime();

	for (int i = 0; i < piecesPerLine; i++)
	{
		for (int j = 0; j < piecesPerColumn; j++)
		{
			if (i % 2 == 0 && j % 2 == 0 || i % 2 != 0 && j % 2 != 0)
			{
				DrawRectangle(backgroundPieces[i][j].x - (gridSize - sizes::pieces) / 2, backgroundPieces[i][j].y - (gridSize - sizes::pieces) / 2, sizes::backgroundRecs, sizes::backgroundRecs, BEIGE);
			}
			else
			{
				DrawRectangle(backgroundPieces[i][j].x - (gridSize - sizes::pieces) / 2, backgroundPieces[i][j].y - (gridSize - sizes::pieces) / 2, sizes::backgroundRecs, sizes::backgroundRecs, DARKBROWN);
			}
		}
	}

	if (initialDraw)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::ATTACK) DrawTexture(texture::attack, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::ATTACK2) DrawTexture(texture::attack2, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::ENERGY) DrawTexture(texture::energy, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COIN) DrawTexture(texture::coin, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::BREAKABLE) DrawTexture(breakable, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::WILDCARD) DrawTexture(texture::wildcard, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERVIOLET) DrawTexture(texture::colorPickerViolet, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERGREEN) DrawTexture(texture::colorPickerGreen, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERRED) DrawTexture(texture::colorPickerRed, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERYELLOW) DrawTexture(texture::colorPickerYellow, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::BOMB) DrawTexture(texture::bomb, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::TNTVERTICAL) DrawTexture(texture::tntVertical, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);
				else if (piece[i][j].kind == Kind::TNTHORIZONTAL) DrawTexture(texture::tntHorizontal, piece[i][j].pos.x, piece[i][j].initialPosY, piece[i][j].color);

				#if DEBUG
				DrawRectangleLines(piece[i][j].pos.x, piece[i][j].initialPosY, sizes::pieces, sizes::pieces, BLACK);
				#endif

				if (piece[i][j].initialPosY >= piece[i][j].pos.y)
				{
					piece[i][j].initialPosY = piece[i][j].pos.y;
				}
				else
				{
					if (!pause)
					{
						speedPieces += speedLerp = speedLerp * speedLerp * speedLerp * (speedLerp * (speedLerp2 * speedLerp - speedLerp3) + speedLerp4);
						piece[i][j].initialPosY += speedPieces;
					}
				}

				if (piece[0][0].initialPosY == piece[0][0].pos.y) introduction = false;
			}
		}
	}
	else
	{
		for (int i = 0; i < piecesPerLine; i++)
		{
			for (int j = 0; j < piecesPerColumn; j++)
			{
				if (piece[i][j].selected) piece[i][j].color = GRAY;
				else piece[i][j].color = WHITE;

				if (piece[i][j].kind == Kind::ATTACK) DrawTexture(texture::attack, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::ATTACK2) DrawTexture(texture::attack2, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::ENERGY) DrawTexture(texture::energy, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COIN) DrawTexture(texture::coin, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::BREAKABLE) DrawTexture(breakable, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::WILDCARD) DrawTexture(texture::wildcard, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERVIOLET) DrawTexture(texture::colorPickerViolet, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERGREEN) DrawTexture(texture::colorPickerGreen, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERRED) DrawTexture(texture::colorPickerRed, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::COLORPICKERYELLOW) DrawTexture(texture::colorPickerYellow, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::BOMB) DrawTexture(texture::bomb, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::TNTVERTICAL) DrawTexture(texture::tntVertical, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);
				else if (piece[i][j].kind == Kind::TNTHORIZONTAL) DrawTexture(texture::tntHorizontal, piece[i][j].pos.x, piece[i][j].pos.y, piece[i][j].color);

				#if DEBUG
				DrawRectangleLines(piece[i][j].pos.x, piece[i][j].pos.y, sizes::pieces, sizes::pieces, BLACK);
				#endif
			}
		}
	}
}

void drawBackground()
{
	if (game::currentScene == game::Scene::GAMEPLAY)
	{
		if (actualBackground == 1) DrawTexture(texture::background, 0, 0, WHITE);
		else if (actualBackground == 2) DrawTexture(texture::background2, 0, 0, WHITE);
		else if (actualBackground == 3) DrawTexture(texture::background3, 0, 0, WHITE);
		else if (actualBackground == 4) DrawTexture(texture::background4, 0, 0, WHITE);
		else if (actualBackground == 5) DrawTexture(texture::background5, 0, 0, WHITE);
		else if (actualBackground == 6) DrawTexture(texture::background6, 0, 0, WHITE);
		else if (actualBackground == 7) DrawTexture(texture::background7, 0, 0, WHITE);
	}
	else DrawTexture(texture::backgroundMenu, 0, 0, WHITE);
}

void drawCharacters()
{
	if (girl.attackStatus) DrawTextureRec(texture::characterGirlThrow, { frameWidth2 * frameCharacterGirlThrow, 0, frameWidth2, (float)texture::characterGirlThrow.height }, { pos::character1ThrowPosX, pos::characterPosY }, WHITE);
	else if (girl.attack2Status) DrawTextureRec(texture::characterGirlThrow2, { frameWidth3 * frameCharacterGirlThrow2, 0, frameWidth3, (float)texture::characterGirlThrow2.height }, { pos::character1ThrowPosX, pos::characterPosY }, WHITE);
	else if (girl.deadStatus) DrawTextureRec(texture::characterGirlDead, { frameWidth4 * frameCharacterGirlDead, 0, frameWidth4, (float)texture::characterGirlDead.height }, { pos::character1DeadPosX, pos::characterPosY }, WHITE);
	else DrawTextureRec(texture::characterGirl, { frameWidth * frameCharacterGirl, 0, frameWidth, (float)texture::characterGirl.height }, { pos::character1PosX, pos::characterPosY }, WHITE);

	if (boy.attackStatus) DrawTextureRec(texture::characterBoyThrow, { frameWidth6 * frameCharacterBoyThrow, 0, frameWidth6, (float)texture::characterBoyThrow.height }, { pos::character2ThrowPosX, pos::characterPosY }, WHITE);
	else if (boy.attack2Status) DrawTextureRec(texture::characterBoyThrow2, { frameWidth7 * frameCharacterBoyThrow2, 0, frameWidth7, (float)texture::characterBoyThrow2.height }, { pos::character2ThrowPosX, pos::characterPosY }, WHITE);
	else if (boy.deadStatus) DrawTextureRec(texture::characterBoyDead, { frameWidth8 * frameCharacterBoyDead, 0, frameWidth8, (float)texture::characterBoyDead.height }, { pos::character2DeadPosX, pos::characterPosY }, WHITE);
	else DrawTextureRec(texture::characterBoy, { frameWidth5 * frameCharacterBoy, 0, frameWidth5, (float)texture::characterBoy.height }, { pos::character2PosX, pos::characterPosY }, WHITE);

	if (girl.kunai) DrawTexture(texture::kunaiGirl, kunaiGirlActualPos, pos::kunaiPosY, WHITE);
	if (boy.kunai) DrawTexture(texture::kunaiBoy, kunaiBoyActualPos, pos::kunaiPosY, WHITE);

	#if DEBUG
	if (girl.attackStatus) DrawRectangleLines(pos::character1ThrowPosX, pos::characterPosY, texture::characterGirlThrow.width / sizes::charactersFrames, texture::characterGirlThrow.height, BLACK);
	else if (girl.attack2Status) DrawRectangleLines(pos::character1ThrowPosX, pos::characterPosY, texture::characterGirlThrow2.width / sizes::charactersFrames, texture::characterGirlThrow2.height, BLACK);
	else if (girl.deadStatus) DrawRectangleLines(pos::character1DeadPosX, pos::characterPosY, texture::characterGirlDead.width / sizes::characterDeadFrames, texture::characterGirlDead.height, BLACK);
	else DrawRectangleLines(pos::character1PosX, pos::characterPosY, texture::characterGirl.width / sizes::charactersFrames, texture::characterGirl.height, BLACK);

	if (boy.attackStatus) DrawRectangleLines(pos::character2ThrowPosX, pos::characterPosY, texture::characterBoyThrow.width / sizes::charactersFrames, texture::characterBoyThrow.height, BLACK);
	else if (boy.attack2Status) DrawRectangleLines(pos::character2ThrowPosX, pos::characterPosY, texture::characterBoyThrow2.width / sizes::charactersFrames, texture::characterBoyThrow2.height, BLACK);
	else if (boy.deadStatus) DrawRectangleLines(pos::character2DeadPosX, pos::characterPosY, texture::characterBoyDead.width / sizes::characterDeadFrames, texture::characterBoyDead.height, BLACK);
	else DrawRectangleLines(pos::character2PosX, pos::characterPosY, texture::characterBoy.width / sizes::charactersFrames, texture::characterBoy.height, BLACK);

	if (girl.kunai) DrawRectangleLines(kunaiGirlActualPos, pos::kunaiPosY, texture::kunaiGirl.width, texture::kunaiGirl.height, BLACK);
	if (boy.kunai) DrawRectangleLines(kunaiBoyActualPos, pos::kunaiPosY, texture::kunaiBoy.width, texture::kunaiBoy.height, BLACK);
	#endif
}

void drawScreenDetails()
{
	if (pause)
	{
		DrawText("Pause", (screenWidth / 2) - MeasureText("Pause", sizes::text3 / 2), pos::pausePosY, sizes::text3, screenDetails);
		DrawTexture(texture::settingsButton2, pos::buttonsLeft, pos::buttonsUp, settingsButtonStatus);
		DrawTexture(texture::menuButton, pos::buttonsRight, pos::buttonsUp, menuButtonStatus);
	}
	else
	{
		DrawCircleGradient(pos::circlePosX, pos::circlePosY, sizes::circleRadius, WHITE, YELLOW);
		DrawText(TextFormat("%06i", score), pos::scorePosX, pos::scorePosY, sizes::text4, BLACK);

		DrawText(TextFormat("Regenerate grid: %i", regerationGrid), (screenWidth / 2) - MeasureText("Regenerate grid: 3", sizes::text4 / 2), texture::background.height - 23, sizes::text4, GRAY);

		DrawRectangle(pos::recPosX, pos::recPosY, girl.live, sizes::recHeight, GREEN);
		DrawRectangle(pos::recPosX, pos::recPosY * 2 - sizes::recLines, girl.shield, sizes::recHeight, SKYBLUE);
		DrawRectangle(pos::recPosX, pos::recPosY * 3 - sizes::recLines * 2, girl.attack, sizes::recHeight, RED);
		DrawRectangle(pos::recPosX, pos::recPosY * 4 - sizes::recLines * 3, girl.attack2, sizes::recHeight, VIOLET);

		DrawRectangle(screenWidth - pos::recPosX - sizes::recWidth1 + (sizes::recWidth1 - boy.live), pos::recPosY, boy.live, sizes::recHeight, GREEN);
		DrawRectangle(screenWidth - pos::recPosX - sizes::recWidth2 + (sizes::recWidth2 - boy.shield), pos::recPosY * 2 - sizes::recLines, boy.shield, sizes::recHeight, SKYBLUE);
		DrawRectangle(screenWidth - pos::recPosX - sizes::recWidth3 + (sizes::recWidth3 - boy.attack), pos::recPosY * 3 - sizes::recLines * 2, boy.attack, sizes::recHeight, RED);
		DrawRectangle(screenWidth - pos::recPosX - sizes::recWidth3 + (sizes::recWidth3 - boy.attack2), pos::recPosY * 4 - sizes::recLines * 3, boy.attack2, sizes::recHeight, VIOLET);

		DrawRectangleLinesEx({ pos::recPosX, pos::recPosY, sizes::recWidth1, sizes::recHeight }, sizes::recLines, screenDetails);
		DrawRectangleLinesEx({ pos::recPosX, pos::recPosY * 2 - sizes::recLines, sizes::recWidth2, sizes::recHeight }, sizes::recLines, screenDetails);
		DrawRectangleLinesEx({ pos::recPosX, pos::recPosY * 3 - sizes::recLines * 2, sizes::recWidth3, sizes::recHeight }, sizes::recLines, screenDetails);
		DrawRectangleLinesEx({ pos::recPosX, pos::recPosY * 4 - sizes::recLines * 3, sizes::recWidth3, sizes::recHeight }, sizes::recLines, screenDetails);

		DrawRectangleLinesEx({ screenWidth - pos::recPosX - sizes::recWidth1, pos::recPosY, sizes::recWidth1, sizes::recHeight }, sizes::recLines, screenDetails);
		DrawRectangleLinesEx({ screenWidth - pos::recPosX - sizes::recWidth2, pos::recPosY * 2 - sizes::recLines, sizes::recWidth2, sizes::recHeight }, sizes::recLines, screenDetails);
		DrawRectangleLinesEx({ screenWidth - pos::recPosX - sizes::recWidth3, pos::recPosY * 3 - sizes::recLines * 2, sizes::recWidth3, sizes::recHeight }, sizes::recLines, screenDetails);
		DrawRectangleLinesEx({ screenWidth - pos::recPosX - sizes::recWidth3, pos::recPosY * 4 - sizes::recLines * 3, sizes::recWidth3, sizes::recHeight }, sizes::recLines, screenDetails);

		if (introduction)
		{
			DrawText("[SPACE] Pause", (screenWidth / 2) - MeasureText("[SPACE] pause", sizes::text / 2), pos::pausePosY2, sizes::text, screenDetails);
			DrawText("[ENTER] Regerate grid", (screenWidth / 2) - MeasureText("[ENTER] Regerate grid", sizes::text / 2), pos::pausePosY3, sizes::text, screenDetails);
		}
	}
}