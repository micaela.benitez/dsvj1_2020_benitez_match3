#ifndef SELECTION_H
#define SELECTION_H

#include "raylib.h"

#include "functions_library/selection/node.h"

void newSelection(Node*& pile, int pieceNumber, int i, int j, Vector2 gridPos, Vector2 pos, Kind kind);
void quitSelection(Node*& pile, int& pieceNumber);

#endif