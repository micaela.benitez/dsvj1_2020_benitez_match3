#ifndef NODE_H
#define NODE_H

#include "raylib.h"

#include "functions_library/entities/entities.h"

struct Node
{
	int pieceNumber;
	int indexI;
	int indexJ;
	Vector2 pos;
	Vector2 gridPos;
	Kind kindPiece;
	Node* next;
};

#endif