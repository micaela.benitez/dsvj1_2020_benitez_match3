#include "selection.h"

void newSelection(Node*& pile, int pieceNumber, int i, int j, Vector2 gridPos, Vector2 pos, Kind kind)
{
	Node* newNode = new Node();
	newNode->pieceNumber = pieceNumber;
	newNode->indexI = i;
	newNode->indexJ = j;
	newNode->gridPos = gridPos;
	newNode->kindPiece = kind;
	newNode->next = pile;
	pile = newNode;
}

void quitSelection(Node*& pile, int& pieceNumber)
{
	Node* aux = pile;
	pieceNumber = aux->pieceNumber;
	pile = aux->next;
	delete aux;
}