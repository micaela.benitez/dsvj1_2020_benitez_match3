#ifndef ENTITIES_MOTION_H
#define ENTITIES_MOTION_H

#include "functions_library/selection/selection.h"
#include "scenes/gameplay/gameplay.h"

void initPieces();
void frames();
void selectionPieces();
void breakablePieceStatus(int indexI, int indexJ);
void specialPieces(int indexI, int indexJ);
void removePieces();
void deselectionPieces();
void movePieces();
void checkNewMatches();
void generateNewPieces();
void updateBars();
void fightCharacters();
void updateScreenDetails();

#endif