#include "entities_motion.h"

using namespace match3;
using namespace gameplay;

#include <math.h>

void initPieces()
{
	for (int i = 0; i < piecesPerLine; i++)
	{
		for (int j = 0; j < piecesPerColumn; j++)
		{
			if (!changedResolution)
			{
				piece[i][j].number = j + (i * piecesPerColumn);

				piece[i][j].selected = false;
				piece[i][j].special = false;
				piece[i][j].regenerated = false;
				piece[i][j].newMatch = false;

				piece[i][j].color = WHITE;

				piece[i][j].kind = (Kind)(GetRandomValue(0, 3));

				piece[i][j].posGrid.x = j;
				piece[i][j].posGrid.y = i;
			}

			piece[i][j].pos.x = j * gridSize + (gridSize - sizes::pieces);
			piece[i][j].initialPosY = (i * gridSize + (gridSize - sizes::pieces)) + sizes::pieces;
			piece[i][j].pos.y = screenHeight - piece[i][j].initialPosY;

			piece[i][j].auxPosY = piece[i][j].pos.y;

			backgroundPieces[i][j].x = piece[i][j].pos.x;
			backgroundPieces[i][j].y = piece[i][j].pos.y;
		}
	}

	// Generate specials pieces
	int indexI = 0;
	int indexJ = 0;

	for (int i = 0; i < totalQuantitySpecialPieces; i++)
	{
		specialPiece = (Kind)(GetRandomValue(4, 12));

		indexI = GetRandomValue(0, piecesPerLine - 1);
		indexJ = GetRandomValue(0, piecesPerColumn - 1);

		piece[indexI][indexJ].kind = specialPiece;
		if (piece[indexI][indexJ].kind != Kind::WILDCARD) piece[indexI][indexJ].special = true;
	}
}

void frames()
{
	static const float speedCharacterFrames = 0.1f;
	static const float speedCharacterDeadFrames = 0.15f;
	
	if (girl.attackStatus)   // Character 1 throw
	{
		frameWidth2 = (float)(texture::characterGirlThrow.width / sizes::charactersFrames);
		maxFrames2 = (int)(texture::characterGirlThrow.width / (int)frameWidth2);
		timerCharacterGirlThrow += GetFrameTime();
		if (timerCharacterGirlThrow >= speedCharacterFrames)
		{
			timerCharacterGirlThrow = 0.0f;
			frameCharacterGirlThrow++;
			cantFrames++;
		}
		frameCharacterGirlThrow = frameCharacterGirlThrow % maxFrames2;
		if (cantFrames == sizes::charactersFrames)
		{
			girl.attackStatus = false;
			kunaiGirlActualPos = pos::kunaiGirlInitialPos;
			girl.kunai = true;
		}
	}
	else if (girl.attack2Status)   // Character 1 throw2
	{
		frameWidth3 = (float)(texture::characterGirlThrow2.width / sizes::charactersFrames);
		maxFrames3 = (int)(texture::characterGirlThrow2.width / (int)frameWidth3);
		timerCharacterGirlThrow2 += GetFrameTime();
		if (timerCharacterGirlThrow2 >= speedCharacterFrames)
		{
			timerCharacterGirlThrow2 = 0.0f;
			frameCharacterGirlThrow2++;
			cantFrames++;
		}
		frameCharacterGirlThrow2 = frameCharacterGirlThrow2 % maxFrames3;
		if (cantFrames == sizes::charactersFrames)
		{
			girl.attack2Status = false;
			kunaiGirlActualPos = pos::kunaiGirlInitialPos;
			girl.kunai = true;
		}
	}	
	else if (girl.deadStatus)   // Character 1 dead
	{
		frameWidth4 = (float)(texture::characterGirlDead.width / sizes::characterDeadFrames);
		maxFrames4 = (int)(texture::characterGirlDead.width / (int)frameWidth4);
		timerCharacterGirlDead += GetFrameTime();
		if (timerCharacterGirlDead >= speedCharacterDeadFrames)
		{
			timerCharacterGirlDead = 0.0f;
			frameCharacterGirlDead++;
			cantFrames++;
		}
		frameCharacterGirlDead = frameCharacterGirlDead % maxFrames4;
		if (cantFrames == sizes::characterDeadFrames)
		{
			girl.deadStatus = false;
			sounds.fallenCharacter = true;
		}
	}
	else
	{
		// Character 1 normal
		frameWidth = (float)(texture::characterGirl.width / sizes::charactersFrames);
		maxFrames = (int)(texture::characterGirl.width / (int)frameWidth);
		timerCharacterGirl += GetFrameTime();
		if (timerCharacterGirl >= speedCharacterFrames)
		{
			timerCharacterGirl = 0.0f;
			frameCharacterGirl++;
		}
		frameCharacterGirl = frameCharacterGirl % maxFrames;
	}

	
	if (boy.attackStatus)   // Character 2 throw
	{
		frameWidth6 = (float)(texture::characterBoyThrow.width / sizes::charactersFrames);
		maxFrames6 = (int)(texture::characterBoyThrow.width / (int)frameWidth6);
		timerCharacterBoyThrow += GetFrameTime();
		if (timerCharacterBoyThrow >= speedCharacterFrames)
		{
			timerCharacterBoyThrow = 0.0f;
			frameCharacterBoyThrow++;
			cantFrames++;
		}
		frameCharacterBoyThrow = frameCharacterBoyThrow % maxFrames6;
		if (cantFrames == sizes::charactersFrames)
		{
			boy.attackStatus = false;
			kunaiBoyActualPos = pos::kunaiBoyInitialPos;
			boy.kunai = true;
		}
	}	
	else if (boy.attack2Status)   // Character 2 throw 2
	{
		frameWidth7 = (float)(texture::characterBoyThrow2.width / sizes::charactersFrames);
		maxFrames7 = (int)(texture::characterBoyThrow2.width / (int)frameWidth7);
		timerCharacterBoyThrow2 += GetFrameTime();
		if (timerCharacterBoyThrow2 >= speedCharacterFrames)
		{
			timerCharacterBoyThrow2 = 0.0f;
			frameCharacterBoyThrow2++;
			cantFrames++;
		}
		frameCharacterBoyThrow2 = frameCharacterBoyThrow2 % maxFrames7;
		if (cantFrames == sizes::charactersFrames)
		{
			boy.attack2Status = false;
			kunaiBoyActualPos = pos::kunaiBoyInitialPos;
			boy.kunai = true;
		}
	}	
	else if (boy.deadStatus)   // Character 2 dead
	{
		frameWidth8 = (float)(texture::characterBoyDead.width / sizes::characterDeadFrames);
		maxFrames8 = (int)(texture::characterBoyDead.width / (int)frameWidth8);
		timerCharacterBoyDead += GetFrameTime();
		if (timerCharacterBoyDead >= speedCharacterDeadFrames)
		{
			timerCharacterBoyDead = 0.0f;
			frameCharacterBoyDead++;
			cantFrames++;
		}
		frameCharacterBoyDead = frameCharacterBoyDead % maxFrames8;
		if (cantFrames == sizes::characterDeadFrames)
		{
			boy.deadStatus = false;
			sounds.fallenCharacter = true;
		}
	}
	else
	{
		// Character 2 normal
		frameWidth5 = (float)(texture::characterBoy.width / sizes::charactersFrames);
		maxFrames5 = (int)(texture::characterBoy.width / (int)frameWidth5);
		timerCharacterBoy += GetFrameTime();
		if (timerCharacterBoy >= speedCharacterFrames)
		{
			timerCharacterBoy = 0.0f;
			frameCharacterBoy++;
		}
		frameCharacterBoy = frameCharacterBoy % maxFrames5;
	}
}

void selectionPieces()
{
	mousePoint = GetMousePosition();

	if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && !piecesMoving && !piecesRegenerating)
	{
		for (int i = 0; i < piecesPerLine; i++)
		{
			for (int j = 0; j < piecesPerColumn; j++)
			{
				if ((CheckCollisionPointRec(mousePoint, { piece[i][j].pos.x, piece[i][j].pos.y, (float)sizes::pieces, (float)sizes::pieces }) && !piece[i][j].selected && pile == NULL) ||
					(CheckCollisionPointRec(mousePoint, { piece[i][j].pos.x, piece[i][j].pos.y, (float)sizes::pieces, (float)sizes::pieces }) && !piece[i][j].selected 
					&& (pile->kindPiece == Kind::WILDCARD || piece[i][j].kind == pile->kindPiece || piece[i][j].kind == Kind::WILDCARD)	&& 
					((piece[i][j].posGrid.x == pile->gridPos.x && piece[i][j].posGrid.y == pile->gridPos.y + 1) || (piece[i][j].posGrid.x == pile->gridPos.x && piece[i][j].posGrid.y == pile->gridPos.y - 1) ||
					(piece[i][j].posGrid.x == pile->gridPos.x + 1 && piece[i][j].posGrid.y == pile->gridPos.y) || (piece[i][j].posGrid.x == pile->gridPos.x - 1 && piece[i][j].posGrid.y == pile->gridPos.y))))
				{
					if (piece[i][j].kind != Kind::BREAKABLE)
					{
						piece[i][j].selected = true;						
						if (quantitySelectedPieces > 1 && piece[i][j].kind == Kind::WILDCARD)
						{
							if (pile->kindPiece == Kind::ATTACK) newSelection(pile, piece[i][j].number, i, j, piece[i][j].posGrid, piece[i][j].pos, Kind::ATTACK);
							else if (pile->kindPiece == Kind::ATTACK2) newSelection(pile, piece[i][j].number, i, j, piece[i][j].posGrid, piece[i][j].pos, Kind::ATTACK2);
							else if (pile->kindPiece == Kind::ENERGY) newSelection(pile, piece[i][j].number, i, j, piece[i][j].posGrid, piece[i][j].pos, Kind::ENERGY);
							else if (pile->kindPiece == Kind::COIN) newSelection(pile, piece[i][j].number, i, j, piece[i][j].posGrid, piece[i][j].pos, Kind::COIN);
						}
						else newSelection(pile, piece[i][j].number, i, j, piece[i][j].posGrid, piece[i][j].pos, piece[i][j].kind);
						initialDraw = false;
						introduction = false;
						sounds.pieceSelected = true;
					}
				}
			}
		}
	}
}

void deselectionPieces()
{
	mousePoint = GetMousePosition();

	if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && quantitySelectedPieces > 1)
	{
		for (int i = 0; i < piecesPerLine; i++)
		{
			for (int j = 0; j < piecesPerColumn; j++)
			{
				if ((CheckCollisionPointRec(mousePoint, { piece[i][j].pos.x, piece[i][j].pos.y, (float)sizes::pieces, (float)sizes::pieces })) && piece[i][j].number == pile->next->pieceNumber && !piece[i][j].special)
				{
					piece[pile->indexI][pile->indexJ].selected = false;
					quitSelection(pile, piece[pile->indexI][pile->indexJ].number);
					sounds.pieceDeselected = true;
				}
				else if ((CheckCollisionPointRec(mousePoint, { piece[i][j].pos.x, piece[i][j].pos.y, (float)sizes::pieces, (float)sizes::pieces })) && piece[i][j].selected && !piece[i][j].special)
				{
					int indexI = i;
					int indexJ = j;

					while (indexI != pile->indexI || indexJ != pile->indexJ)
					{
						piece[pile->indexI][pile->indexJ].selected = false;
						quitSelection(pile, piece[pile->indexI][pile->indexJ].number);
						sounds.pieceDeselected = true;
					}
				}
			}
		}
	}
}

void breakablePieceStatus(int indexI, int indexJ)
{
	if (hitsBreakable == 0)
	{
		breakable = texture::breakable2;
		hitsBreakable++;
	}
	else if (hitsBreakable == 1)
	{
		breakable = texture::breakable3;
		hitsBreakable++;
	}
	else
	{
		breakable = texture::breakable1;
		hitsBreakable = 0;
		piece[indexI][indexJ].kind = Kind::EMPTY;
		piece[indexI][indexJ].special = false;
	}
}

void specialPieces(int indexI, int indexJ)
{
	if (piece[indexI][indexJ].kind == Kind::COLORPICKERVIOLET)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::ATTACK2)
				{
					if (girl.attack2 < sizes::recWidth3) girl.attack2 += points::attack2Piece;
					piece[i][j].kind = Kind::EMPTY;
					sounds.bomb = true;
				}
			}
		}
	}
	else if (piece[indexI][indexJ].kind == Kind::COLORPICKERGREEN)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::ENERGY)
				{
					if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
					else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
					piece[i][j].kind = Kind::EMPTY;
					sounds.bomb = true;
				}
			}
		}
	}
	else if (piece[indexI][indexJ].kind == Kind::COLORPICKERRED)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::ATTACK)
				{
					if (girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
					piece[i][j].kind = Kind::EMPTY;
					sounds.bomb = true;
				}
			}
		}
	}
	else if (piece[indexI][indexJ].kind == Kind::COLORPICKERYELLOW)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::COIN)
				{
					piece[i][j].kind = Kind::EMPTY;
					score += points::score;
					sounds.bomb = true;
				}
			}
		}
	}
	else if (piece[indexI][indexJ].kind == Kind::BOMB)
	{
		if (indexI < piecesPerLine - 1)
		{
			if (piece[indexI + 1][indexJ].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI + 1][indexJ].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI + 1][indexJ].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI + 1][indexJ].kind == Kind::COIN) score += points::score;
			piece[indexI + 1][indexJ].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexI > 0)
		{
			if (piece[indexI - 1][indexJ].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI - 1][indexJ].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI - 1][indexJ].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI - 1][indexJ].kind == Kind::COIN) score += points::score;
			piece[indexI - 1][indexJ].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexJ < piecesPerColumn - 1)
		{
			if (piece[indexI][indexJ + 1].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI][indexJ + 1].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI][indexJ + 1].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI][indexJ + 1].kind == Kind::COIN) score += points::score;
			piece[indexI][indexJ + 1].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexJ > 0)
		{
			if (piece[indexI][indexJ - 1].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI][indexJ - 1].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI][indexJ - 1].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI][indexJ - 1].kind == Kind::COIN) score += points::score;
			piece[indexI][indexJ - 1].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexI < piecesPerLine - 1 && indexJ < piecesPerColumn - 1)
		{
			if (piece[indexI + 1][indexJ + 1].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI + 1][indexJ + 1].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI + 1][indexJ + 1].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI + 1][indexJ + 1].kind == Kind::COIN) score += points::score;
			piece[indexI + 1][indexJ + 1].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexI > 0 && indexJ > 0)
		{
			if (piece[indexI - 1][indexJ - 1].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI - 1][indexJ - 1].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI - 1][indexJ - 1].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI - 1][indexJ - 1].kind == Kind::COIN) score += points::score;
			piece[indexI - 1][indexJ - 1].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexI < piecesPerLine - 1 && indexJ > 0)
		{
			if (piece[indexI + 1][indexJ - 1].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI + 1][indexJ - 1].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI + 1][indexJ - 1].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI + 1][indexJ - 1].kind == Kind::COIN) score += points::score;
			piece[indexI + 1][indexJ - 1].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
		if (indexI > 0 && indexJ < piecesPerColumn - 1)
		{
			if (piece[indexI - 1][indexJ + 1].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI - 1][indexJ + 1].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI - 1][indexJ + 1].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI - 1][indexJ + 1].kind == Kind::COIN) score += points::score;
			piece[indexI - 1][indexJ + 1].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
	}
	else if (piece[indexI][indexJ].kind == Kind::TNTVERTICAL)
	{
		for (int r = 0; r < piecesPerLine; r++)
		{
			if (piece[r][indexJ].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[r][indexJ].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[r][indexJ].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[r][indexJ].kind == Kind::COIN) score += points::score;
			piece[r][indexJ].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
	}
	else if (piece[indexI][indexJ].kind == Kind::TNTHORIZONTAL)
	{
		for (int r = 0; r < piecesPerColumn; r++)
		{

			if (piece[indexI][r].kind == Kind::ENERGY)
			{
				if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
				else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
			}
			else if (piece[indexI][r].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
			else if (piece[indexI][r].kind == Kind::ATTACK2 && girl.shield < sizes::recWidth2) girl.attack2 += points::attack2Piece;
			else if (piece[indexI][r].kind == Kind::COIN) score += points::score;
			piece[indexI][r].kind = Kind::EMPTY;
			sounds.bomb = true;
		}
	}
}

void removePieces()
{
	quantitySelectedPieces = 0;

	for (int i = piecesPerLine - 1; i >= 0; i--)
	{
		for (int j = piecesPerColumn - 1; j >= 0; j--)
		{
			if (piece[i][j].selected) quantitySelectedPieces++;
		}
	}

	if (IsMouseButtonUp(MOUSE_LEFT_BUTTON))
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].selected)
				{
					piece[i][j].selected = false;
					quitSelection(pile, piece[i][j].number);

					if ((quantitySelectedPieces >= minimumSelectedPieces && !piece[i][j].special) || piece[i][j].special)
					{
						// If there are breakable pieces around
						if (i < piecesPerLine - 1) if (piece[i + 1][j].kind == Kind::BREAKABLE) breakablePieceStatus(i + 1, j);
						if (i > 0) if (piece[i - 1][j].kind == Kind::BREAKABLE) breakablePieceStatus(i - 1, j);
						if (j < piecesPerColumn - 1) if (piece[i][j + 1].kind == Kind::BREAKABLE) breakablePieceStatus(i, j + 1);
						if (j > 0) if (piece[i][j - 1].kind == Kind::BREAKABLE) breakablePieceStatus(i, j - 1);

						// If the piece that breaks is a special one
						if (piece[i][j].special) specialPieces(i, j);

						if (piece[i][j].kind == Kind::ENERGY)
						{
							if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
							else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
						}
						else if (piece[i][j].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
						else if (piece[i][j].kind == Kind::ATTACK2 && girl.attack2 < sizes::recWidth2) girl.attack2 += points::attack2Piece;
						else if (piece[i][j].kind == Kind::COIN) score++;

						piece[i][j].kind = Kind::EMPTY;
						sounds.piecesDisappear = true;
					}
					else sounds.wrongSelection = true;
				}
				else if (piece[i][j].newMatch)
				{
					// If there are breakable pieces around
					if (i < piecesPerLine - 1) if (piece[i + 1][j].kind == Kind::BREAKABLE) breakablePieceStatus(i + 1, j);
					if (i > 0) if (piece[i - 1][j].kind == Kind::BREAKABLE) breakablePieceStatus(i - 1, j);
					if (j < piecesPerColumn - 1) if (piece[i][j + 1].kind == Kind::BREAKABLE) breakablePieceStatus(i, j + 1);
					if (j > 0) if (piece[i][j - 1].kind == Kind::BREAKABLE) breakablePieceStatus(i, j - 1);

					if (piece[i][j].kind == Kind::ENERGY)
					{
						if (girl.live < sizes::recWidth1) girl.live += points::eneryPiece;
						else if (girl.live >= sizes::recWidth1 && (girl.shield < sizes::recWidth1)) girl.shield += points::eneryPiece;
					}
					else if (piece[i][j].kind == Kind::ATTACK && girl.attack < sizes::recWidth3) girl.attack += points::attackPiece;
					else if (piece[i][j].kind == Kind::ATTACK2 && girl.attack2 < sizes::recWidth2) girl.attack2 += points::attack2Piece;
					else if (piece[i][j].kind == Kind::COIN) score++;

					piece[i][j].newMatch = false;
					piece[i][j].kind = Kind::EMPTY;
				}
			}
		}

		Node* aux;

		while (pile != NULL)
		{
			aux = pile;
			pile = aux->next;
			delete(aux);
		}
	}
}

void movePieces()
{
	piecesMoving = false;

	float speedLerp = 25.25f * GetFrameTime();
	static const float speedLerp2 = 206.0f * GetFrameTime();
	static const float speedLerp3 = 215.0f * GetFrameTime();
	static const float speedLerp4 = 210.0f * GetFrameTime();

	for (int r = 0; r < piecesPerLine; r++)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::EMPTY && i < piecesPerLine - 1)
				{
					if (piece[i + 1][j].pos.y >= piece[i][j].pos.y)
					{
						piece[i][j].kind = piece[i + 1][j].kind;
						piece[i][j].special = piece[i + 1][j].special;
						piece[i + 1][j].kind = Kind::EMPTY;
						piece[i + 1][j].special = false;
						piece[i + 1][j].pos.y = piece[i + 1][j].auxPosY;
						speedPieces = 0;
						sounds.fallenPiece = true;
					}
					else 
					{
						speedPieces += speedLerp = speedLerp * speedLerp * speedLerp * (speedLerp * (speedLerp2 * speedLerp - speedLerp3) + speedLerp4);
						piece[i + 1][j].pos.y += speedPieces;
						if (piece[i + 1][j].kind != Kind::EMPTY) piecesMoving = true;
					}					
				}
			}
		}
	}

}

void checkNewMatches()
{
	Pieces aux;
	int auxAmont = 1;

	for (int i = piecesPerLine - 1; i >= 0; i--)
	{
		for (int j = piecesPerColumn - 1; j >= 0; j--)
		{
			if (j == piecesPerColumn - 1 && i == piecesPerLine - 1)
			{
				aux.kind = piece[i][j].kind;
				piece[i][j].newMatch = true;
			}
			else if (aux.kind == piece[i][j].kind)
			{
				auxAmont++;
				newSelection(pile, piece[i][j].number, i, j, piece[i][j].posGrid, piece[i][j].pos, piece[i][j].kind);
				piece[i][j].newMatch = true;
			}
			else
			{
				if (auxAmont >= minimumSelectedPieces) removePieces();

				for (int i = piecesPerLine - 1; i >= 0; i--)
				{
					for (int j = piecesPerColumn - 1; j >= 0; j--)
					{
						piece[i][j].newMatch = false;
					}
				}

				auxAmont = 1;
				aux.kind = piece[i][j].kind;
				piece[i][j].newMatch = true;
			}
		}
	}

	if (auxAmont >= minimumSelectedPieces) removePieces();

	for (int i = piecesPerLine - 1; i >= 0; i--)
	{
		for (int j = piecesPerColumn - 1; j >= 0; j--)
		{
			piece[i][j].newMatch = false;
		}
	}
}

void generateNewPieces()
{
	piecesRegenerating = false;

	float speedLerp = 25.25f * GetFrameTime();
	static const float speedLerp2 = 206.0f * GetFrameTime();
	static const float speedLerp3 = 215.0f * GetFrameTime();
	static const float speedLerp4 = 210.0f * GetFrameTime();

	quantitySpecialPieces = 0;

	for (int i = 0; i < piecesPerLine; i++)   // How many special pieces are active
	{
		for (int j = 0; j < piecesPerColumn; j++)
		{
			if (piece[i][j].kind != Kind::ATTACK && piece[i][j].kind != Kind::ATTACK2 && piece[i][j].kind != Kind::ENERGY && piece[i][j].kind != Kind::COIN && piece[i][j].kind != Kind::EMPTY) quantitySpecialPieces++;
		}
	}

	if (!piecesMoving)
	{
		for (int i = piecesPerLine - 1; i >= 0; i--)
		{
			for (int j = piecesPerColumn - 1; j >= 0; j--)
			{
				if (piece[i][j].kind == Kind::EMPTY)
				{
					if (quantitySpecialPieces < totalQuantitySpecialPieces)
					{
						piece[i][j].kind = (Kind)(GetRandomValue(0, 12));
						if (piece[i][j].kind != Kind::ATTACK && piece[i][j].kind != Kind::ATTACK2 && piece[i][j].kind != Kind::ENERGY && piece[i][j].kind != Kind::COIN)
						{
							quantitySpecialPieces++;
							if (piece[i][j].kind != Kind::WILDCARD) piece[i][j].special = true;
							else piece[i][j].special = false;
						}
						else piece[i][j].special = false;
					}
					else
					{
						piece[i][j].kind = (Kind)(GetRandomValue(0, 3));
						piece[i][j].special = false;
					}

					piece[i][j].pos.y -= 100;
					piece[i][j].auxPosY = screenHeight - ((i * gridSize + (gridSize - sizes::pieces)) + sizes::pieces);
					piece[i][j].regenerated = true;
				}
			}
		}
	}

	for (int i = piecesPerLine - 1; i >= 0; i--)
	{
		for (int j = piecesPerColumn - 1; j >= 0; j--)
		{
			if (piece[i][j].regenerated)
			{
				if (piece[i][j].pos.y >= piece[i][j].auxPosY)
				{
					piece[i][j].pos.y = piece[i][j].auxPosY;
					piece[i][j].regenerated = false;
					sounds.fallenPiece = true;
					checkNewMatches();
				}
				else
				{
					if (!pause)
					{
						speedPieces += speedLerp = speedLerp * speedLerp * speedLerp * (speedLerp * (speedLerp2 * speedLerp - speedLerp3) + speedLerp4);
						piece[i][j].pos.y += speedPieces;
						piecesRegenerating = true;
					}
				}
			}
		}
	}
}

void updateBars()
{
	if (girl.shield <= 0.0f) girl.live -= points::girlLive * GetFrameTime();
	else girl.shield -= points::girlLive * GetFrameTime();

	if (girl.attack >= sizes::recWidth3)
	{
		girl.attack = 0;
		girl.attackStatus = true;
		cantFrames = 1;
		timerCharacterGirlThrow = 0.0f;
		frameCharacterGirlThrow = 0;
		frameWidth2 = 0.0f;
		maxFrames2 = 0;
		points::lostLifeAttack = 50.0f;
	}
	if (girl.attack2 >= sizes::recWidth3)
	{
		girl.attack2 = 0;
		girl.attack2Status = true;
		cantFrames = 1;
		timerCharacterGirlThrow2 = 0.0f;
		frameCharacterGirlThrow2 = 0;
		frameWidth3 = 0.0f;
		maxFrames3 = 0;
		points::lostLifeAttack = 70.0f;
	}

	if (boy.attack < sizes::recWidth3) boy.attack += points::boyAttack * GetFrameTime();
	else
	{
		boy.attack = 0;
		boy.attackStatus = true;
		cantFrames = 1;
		timerCharacterBoyThrow = 0.0f;
		frameCharacterBoyThrow = 0;;
		frameWidth6 = 0.0f;
		maxFrames6 = 0;
		points::lostLifeAttack = 20.0f;
	}
	if (boy.attack2 < sizes::recWidth3) boy.attack2 += points::boyAttack2 * GetFrameTime();
	else
	{
		boy.attack2 = 0;
		boy.attack2Status = true;
		cantFrames = 1;
		timerCharacterBoyThrow2 = 0.0f;
		frameCharacterBoyThrow2 = 0;;
		frameWidth7 = 0.0f;
		maxFrames7 = 0;
		points::lostLifeAttack = 30.0f;
	}

	if (girl.live >= sizes::recWidth1) girl.live = sizes::recWidth1;
	if (girl.shield >= sizes::recWidth2) girl.shield = sizes::recWidth2;
	if (girl.attack >= sizes::recWidth3) girl.attack = sizes::recWidth3;
	if (girl.attack2 >= sizes::recWidth3) girl.attack2 = sizes::recWidth3;

	if (boy.live >= sizes::recWidth1) boy.live = sizes::recWidth1;
	if (boy.shield >= sizes::recWidth2) boy.shield = sizes::recWidth2;
	if (boy.attack >= sizes::recWidth3) boy.attack = sizes::recWidth3;
	if (boy.attack2 >= sizes::recWidth3) boy.attack2 = sizes::recWidth3;
}

void fightCharacters()
{
	static const float kunaiSpeed = 200.0f;
	float auxPoints = 0;

	if (girl.kunai)
	{
		kunaiGirlActualPos += kunaiSpeed * GetFrameTime();
		if (kunaiGirlActualPos >= pos::kunaiGirlFinalPos)
		{
			score += points::score * 5;

			girl.kunai = false;
			boy.deadStatus = true;
			cantFrames = 1;
			timerCharacterBoyDead = 0.0f;
			frameCharacterBoyDead = 0;;
			frameWidth8 = 0.0f;
			maxFrames8 = 0;

			if (boy.shield > points::lostLifeAttack) boy.shield -= points::lostLifeAttack;
			else if (boy.shield > 0.0f && boy.shield < points::lostLifeAttack)
			{
				auxPoints = points::lostLifeAttack - boy.shield;
				boy.shield = 0.0f;
				boy.live -= auxPoints;
			}
			else if (boy.shield <= 0.0f) boy.live -= points::lostLifeAttack;
		}
	}

	if (boy.kunai)
	{
		kunaiBoyActualPos -= kunaiSpeed * GetFrameTime();
		if (kunaiBoyActualPos <= pos::kunaiBoyFinalPos)
		{
			boy.kunai = false;
			girl.deadStatus = true;
			cantFrames = 1;
			timerCharacterGirlDead = 0.0f;
			frameCharacterGirlDead = 0;
			frameWidth4 = 0.0f;
			maxFrames4 = 0;

			if (girl.shield > points::lostLifeAttack) girl.shield -= points::lostLifeAttack;
			else if (girl.shield > 0.0f && girl.shield < points::lostLifeAttack)
			{
				auxPoints = points::lostLifeAttack - girl.shield;
				girl.shield = 0.0f;
				girl.live -= auxPoints;
			}
			else if (girl.shield <= 0.0f) girl.live -= points::lostLifeAttack;
		}
	}
}

void updateScreenDetails()
{
	mousePoint = GetMousePosition();

	if (CheckCollisionPointRec(mousePoint, { pos::buttonsLeft, pos::buttonsUp, (float)texture::settingsButton2.width, (float)texture::settingsButton2.height }))
	{
		settingsButtonStatus = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			game::currentScene = game::Scene::SETTINGS;
			game::previousScene = game::Scene::GAMEPLAY;
		}
	}
	else settingsButtonStatus = WHITE;

	if (CheckCollisionPointRec(mousePoint, { pos::buttonsRight, pos::buttonsUp, (float)texture::menuButton.width, (float)texture::menuButton.height }))
	{
		menuButtonStatus = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			game::currentScene = game::Scene::MENU;
			StopMusicStream(audio::gameMusic.gameplay);
		}
	}
	else menuButtonStatus = WHITE;
}