#ifndef ENTITIES_H
#define ENTITIES_H

#include "raylib.h"

enum class Kind
{ 
	ATTACK,
	ATTACK2, 
	ENERGY, 
	COIN,
	BREAKABLE,
	WILDCARD,
	COLORPICKERVIOLET, 
	COLORPICKERGREEN, 
	COLORPICKERRED, 
	COLORPICKERYELLOW,
	BOMB, 
	TNTVERTICAL,
	TNTHORIZONTAL,
	EMPTY
};

struct Pieces
{
	int number;
	bool selected;
	bool special;
	bool regenerated;
	bool newMatch;
	float initialPosY;
	float auxPosY;
	Vector2 pos;
	Vector2 posGrid;
	Kind kind;
	Color color;
};

struct Characters
{
	float live;
	float shield;
	float attack;
	float attack2;
	bool attackStatus;
	bool attack2Status;
	bool deadStatus;
	bool kunai;
};

#endif