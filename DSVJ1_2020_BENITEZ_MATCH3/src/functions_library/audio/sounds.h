#ifndef SOUNDS_H
#define SOUNDS_H

struct Sounds
{
	bool button;
	bool pieceSelected;
	bool pieceDeselected;
	bool fallenCharacter;
	bool fallenPiece;
	bool piecesDisappear;
	bool bomb;
	bool wrongSelection;
};

#endif