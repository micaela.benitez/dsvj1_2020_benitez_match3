#include "audio.h"

using namespace match3;
using namespace gameplay;

namespace audio
{
	GameMusic gameMusic;
	float musicVolume = 1.0f;

	GameSounds gameSounds;
	float soundVolume = 4.0f;

	void menuAudio()
	{
		UpdateMusicStream(gameMusic.menu);

		if (sounds.button) PlaySoundMulti(gameSounds.button);
	}

	void gameplayAudio()
	{
		UpdateMusicStream(gameMusic.gameplay);

		if (sounds.button) PlaySoundMulti(gameSounds.button);
		if (sounds.pieceSelected) PlaySoundMulti(gameSounds.pieceSelected);
		if (sounds.pieceDeselected) PlaySoundMulti(gameSounds.pieceDeselected);
		if (sounds.fallenCharacter) PlaySoundMulti(gameSounds.fallenCharacter);
		if (sounds.fallenPiece) PlaySoundMulti(gameSounds.fallenPiece);
		if (sounds.piecesDisappear) PlaySoundMulti(gameSounds.piecesDisappear);
		if (sounds.bomb) PlaySoundMulti(gameSounds.bomb);
		if (sounds.wrongSelection) PlaySoundMulti(gameSounds.wrongSelection);
	}

	void loadSounds()
	{
		gameSounds.button = LoadSound("res/assets/audio/sounds/button.wav");
		gameSounds.pieceSelected = LoadSound("res/assets/audio/sounds/selected.wav");
		gameSounds.pieceDeselected = LoadSound("res/assets/audio/sounds/deselected.wav");
		gameSounds.fallenCharacter = LoadSound("res/assets/audio/sounds/fallencharacter.wav");
		gameSounds.fallenPiece = LoadSound("res/assets/audio/sounds/fallenpieces.wav");
		gameSounds.piecesDisappear = LoadSound("res/assets/audio/sounds/piecesdisappear.wav");
		gameSounds.bomb = LoadSound("res/assets/audio/sounds/bomb.wav");
		gameSounds.wrongSelection = LoadSound("res/assets/audio/sounds/wrong.mp3");
	}

	void setSoundsVolume()
	{
		SetSoundVolume(gameSounds.button, soundVolume);
		SetSoundVolume(gameSounds.pieceSelected, soundVolume);
		SetSoundVolume(gameSounds.pieceDeselected, soundVolume);
		SetSoundVolume(gameSounds.fallenCharacter, soundVolume);
		SetSoundVolume(gameSounds.fallenPiece, soundVolume);
		SetSoundVolume(gameSounds.piecesDisappear, soundVolume);
		SetSoundVolume(gameSounds.bomb, soundVolume);
		SetSoundVolume(gameSounds.wrongSelection, soundVolume);
	}

	void unloadSounds()
	{
		UnloadSound(gameSounds.button);
		UnloadSound(gameSounds.pieceSelected);
		UnloadSound(gameSounds.pieceDeselected);
		UnloadSound(gameSounds.fallenCharacter);
		UnloadSound(gameSounds.fallenPiece);
		UnloadSound(gameSounds.piecesDisappear);
		UnloadSound(gameSounds.bomb);
		UnloadSound(gameSounds.wrongSelection);
	}

	void loadMusic()
	{
		gameMusic.menu = LoadMusicStream("res/assets/audio/music/menu.mp3");
		gameMusic.gameplay = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
	}

	void setMusicVolume()
	{
		SetMusicVolume(gameMusic.menu, musicVolume);
		SetMusicVolume(gameMusic.gameplay, musicVolume);
	}

	void unloadMusic()
	{
		UnloadMusicStream(gameMusic.menu);
		UnloadMusicStream(gameMusic.gameplay);
	}
}