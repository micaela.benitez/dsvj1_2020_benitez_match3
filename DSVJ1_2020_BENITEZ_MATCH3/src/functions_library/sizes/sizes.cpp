#include "sizes.h"

using namespace match3;
using namespace gameplay;

namespace sizes
{
	float pieces = screenWidth / 10;
	float backgroundRecs = screenHeight / 12;

	float charactersWidth = screenHeight / 11;
	float charactersWidthThrow = screenHeight / 8;
	float charactersWidthDead = screenHeight / 6;
	float charactersHeight = screenHeight / 6.5f;

	float title = screenHeight / 10;

	float buttonsWidth = screenWidth / 2.5;
	float buttonsHeight = screenHeight / 11;
	float circleButtons = screenWidth / 10;
	
	float text = screenWidth / 23; // 30
	float text2 = screenHeight / 25; // 40
	float text3 = (screenWidth / 23) * 2; // 60
	float text4 = screenHeight / 50;   // 20

	float circleRadius = screenHeight / 24;

	float recWidth1 = screenHeight / 3.7f;
	float recWidth2 = (screenHeight / 3.7f) - (screenHeight / 24);
	float recWidth3 = (screenHeight / 3.7f) - (screenHeight / 24) * 2;
	float recHeight = screenHeight / 50;
	float recLines = screenWidth / 230;

	float kunaiWidth = screenHeight / 12.5f;
	float kunaiHeight = screenHeight / 66;

	// Quantity frames
	float borderFrames = 2;
	float charactersFrames = 10;
	float characterDeadFrames = 5;

	void updateSizes()
	{
		pieces = screenWidth / 10;
		backgroundRecs = screenHeight / 12;

		charactersWidth = screenHeight / 11;
		charactersWidthThrow = screenHeight / 8;
		charactersWidthDead = screenHeight / 6;
		charactersHeight = screenHeight / 6.5f;

		title = screenHeight / 10;

		buttonsWidth = screenWidth / 2.5;
		buttonsHeight = screenHeight / 11;
		circleButtons = screenWidth / 10;

		text = screenWidth / 23; // 30
		text2 = screenHeight / 25; // 40
		text3 = (screenWidth / 23) * 2; // 60
		text4 = screenHeight / 50;   // 20

		circleRadius = screenHeight / 24;

		recWidth1 = screenHeight / 3.7f;
		recWidth2 = (screenHeight / 3.7f) - (screenHeight / 24);
		recWidth3 = (screenHeight / 3.7f) - (screenHeight / 24) * 2;
		recHeight = screenHeight / 50;
		recLines = screenWidth / 230;

		kunaiWidth = screenHeight / 12.5f;
		kunaiHeight = screenHeight / 66;

		// Quantity frames
		borderFrames = 2;
		charactersFrames = 10;
		characterDeadFrames = 5;
	}
}