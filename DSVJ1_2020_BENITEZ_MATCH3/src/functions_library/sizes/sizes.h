#ifndef SIZES_H
#define SIZES_H

#include "scenes/gameplay/gameplay.h"

namespace sizes
{
	extern float pieces;
	extern float backgroundRecs;

	extern float charactersWidth;
	extern float charactersWidthThrow;
	extern float charactersWidthDead;
	extern float charactersHeight;

	extern float title;

	extern float buttonsWidth;
	extern float buttonsHeight;
	extern float circleButtons;

	extern float text;
	extern float text2;
	extern float text3;
	extern float text4;

	extern float circleRadius;

	extern float recWidth1;
	extern float recWidth2;
	extern float recWidth3;
	extern float recHeight;
	extern float recLines;

	extern float kunaiWidth;
	extern float kunaiHeight;

	// Quantity frames
	extern float borderFrames;
	extern float charactersFrames;
	extern float characterDeadFrames;

	void updateSizes();
}

#endif