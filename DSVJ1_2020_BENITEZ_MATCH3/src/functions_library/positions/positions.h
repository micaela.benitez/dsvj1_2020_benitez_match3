#ifndef POSITIONS_H
#define POSITIONS_H

#include "scenes/gameplay/gameplay.h"

namespace pos
{
	extern float buttonsUp;
	extern float buttonsRight;
	extern float buttonsLeft;
	extern float middleButtonsPosX;

	extern float character1PosX;
	extern float character1ThrowPosX;
	extern float character1DeadPosX;
	extern float character2PosX;
	extern float character2ThrowPosX;
	extern float character2DeadPosX;
	extern float characterPosY;

	extern float kunaiGirlInitialPos;
	extern float kunaiGirlFinalPos;
	extern float kunaiBoyInitialPos;
	extern float kunaiBoyFinalPos;

	extern float kunaiPosY;

	extern float titlePoxY;
	extern float titlePoxY2;

	extern float menuPosY;
	extern float menuPosY2;
	extern float menuPosY3;
	extern float menuPosY4;
	extern float menuPosY5;

	extern float creditsPosY;
	extern float creditsPosY2;
	extern float creditsPosY3;
	extern float creditsPosY4;
	extern float creditsPosY5;
	extern float creditsPosY6;
	extern float creditsPosY7;
	extern float creditsPosY8;
	extern float creditsPosY9;
	extern float creditsPosY10;
	extern float creditsPosY11;
	extern float creditsPosY12;
	extern float creditsPosY13;
	extern float creditsPosY14;
	extern float creditsPosY15;

	extern float settingPosY;
	extern float settingPosY2;

	extern float resolutionPosY;
	extern float resolutionPosY2;
	extern float resolutionPosY3;
	extern float resolutionPosY4;

	extern float audioPosY;
	extern float audioPosY2;
	extern float audioPosY3;
	extern float audioPosY4;
	extern float audioPosY5;
	extern float audioPosY6;
	extern float audioPosY7;
	extern float audioPosY8;
	
	extern float resultPosX;
	extern float resultPosX2;
	extern float resultPosY;
	extern float resultPosY2;
	extern float resultPosY3;
	extern float resultPosY4;
	extern float resultPosY5;

	extern float instructionsPosY;
	extern float instructionsPosY2;
	extern float instructionsPosY3;
	extern float instructionsPosY4;
	extern float instructionsPosY5;
	extern float instructionsPosY6;
	extern float instructionsPosY7;
	extern float instructionsPosY8;
	extern float instructionsPosY9;
	extern float instructionsPosY10;
	extern float instructionsPosY11;
	extern float instructionsPosY12;
	extern float instructionsPosY13;

	extern float instructions2PosY;
	extern float instructions2PosY2;
	extern float instructions2PosY3;
	extern float instructions2PosY4;
	extern float instructions2PosY5;
	extern float instructions2PosY6;
	extern float instructions2PosY7;
	extern float instructions2PosY8;
	extern float instructions2PosY9;
	extern float instructions2PosY10;
	extern float instructions2PosY11;
	extern float instructions2PosY12;
	extern float instructions2PosY13;

	extern float instructions3PosX;
	extern float instructions3PosX2;
	extern float instructions3PosX3;
	extern float instructions3PosX4;
	extern float instructions3PosX5;
	extern float instructions3PosX6;
	extern float instructions3PosX7;
	extern float instructions3PosX8;
	extern float instructions3PosX9;
	extern float instructions3PosX10;
	extern float instructions3PosX11;
	extern float instructions3PosX12;
	extern float instructions3PosX13;
	extern float instructions3PosX14;
	extern float instructions3PosX15;
	extern float instructions3PosX16;
	extern float instructions3PosX17;
	extern float instructions3PosX18;

	extern float instructions3PosY;
	extern float instructions3PosY2;
	extern float instructions3PosY3;
	extern float instructions3PosY4;
	extern float instructions3PosY5;
	extern float instructions3PosY6;
	extern float instructions3PosY7;
	extern float instructions3PosY8;
	extern float instructions3PosY9;
	extern float instructions3PosY10;
	extern float instructions3PosY11;

	extern float pausePosY;
	extern float pausePosY2;
	extern float pausePosY3;

	extern float versionPosX;
	extern float versionPosY;

	extern float circlePosX;
	extern float circlePosY;

	extern float scorePosX;
	extern float scorePosY;

	extern float recPosX;
	extern float recPosY;

	void updateVariables();
}

#endif