#include "positions.h"

using namespace match3;
using namespace gameplay;

namespace pos
{
	float buttonsUp = 10;
	float buttonsRight = screenWidth - sizes::circleButtons - 10;
	float buttonsLeft = 10;
	float middleButtonsPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);

	float character1PosX = screenHeight / 10;
	float character1ThrowPosX = character1PosX - 10;
	float character1DeadPosX = character1PosX - 20;
	float character2PosX = screenWidth - screenHeight / 10 - sizes::charactersWidth;
	float character2ThrowPosX = character2PosX - 10;
	float character2DeadPosX = character2PosX - 50;
	float characterPosY = 293;

	float kunaiGirlInitialPos = character1ThrowPosX + 100;
	float kunaiGirlFinalPos = character2ThrowPosX - 50;
	float kunaiBoyInitialPos = character2ThrowPosX - 70;
	float kunaiBoyFinalPos = character1ThrowPosX + 75;

	float kunaiPosY = characterPosY + 80;

	float titlePoxY = (screenHeight / 2) - 400;
	float titlePoxY2 = (screenHeight / 2) - 300;

	float menuPosY = (screenHeight / 2) - 180;
	float menuPosY2 = (screenHeight / 2) - 60;
	float menuPosY3 = (screenHeight / 2) + 60;
	float menuPosY4 = (screenHeight / 2) + 180;
	float menuPosY5 = (screenHeight / 2) + 300;

	float creditsPosY = (screenHeight / 2) - 350;
	float creditsPosY2 = (screenHeight / 2) - 300;
	float creditsPosY3 = (screenHeight / 2) - 150;
	float creditsPosY4 = (screenHeight / 2) - 100;
	float creditsPosY5 = (screenHeight / 2) - 60;
	float creditsPosY6 = (screenHeight / 2) - 20;
	float creditsPosY7 = (screenHeight / 2) + 20;
	float creditsPosY8 = (screenHeight / 2) + 60;
	float creditsPosY9 = (screenHeight / 2) + 100;
	float creditsPosY10 = (screenHeight / 2) + 140;
	float creditsPosY11 = (screenHeight / 2) + 180;
	float creditsPosY12 = (screenHeight / 2) + 220;
	float creditsPosY13 = (screenHeight / 2) + 260;
	float creditsPosY14 = (screenHeight / 2) + 300;
	float creditsPosY15 = (screenHeight / 2) + 340;

	float settingPosY = (screenHeight / 2) - 50 - sizes::buttonsHeight;
	float settingPosY2 = (screenHeight / 2) + 50;

	float resolutionPosY = (screenHeight / 2) - 200;
	float resolutionPosY2 = (screenHeight / 2) - 120;
	float resolutionPosY3 = (screenHeight / 2) - 20;
	float resolutionPosY4 = (screenHeight / 2) + 80;

	float audioPosY = (screenHeight / 2) - 400;
	float audioPosY2 = (screenHeight / 2) - 320;
	float audioPosY3 = (screenHeight / 2) - 220;
	float audioPosY4 = (screenHeight / 2) - 120;
	float audioPosY5 = (screenHeight / 2) + 40;
	float audioPosY6 = (screenHeight / 2) + 120;
	float audioPosY7 = (screenHeight / 2) + 220;
	float audioPosY8 = (screenHeight / 2) + 320;

	float resultPosX = (screenWidth / 2) - (texture::playAgainButton.width / 2);
	float resultPosX2 = (screenWidth / 2) - (texture::playAgainButton.width / 2);
	float resultPosY = (screenHeight / 2) - 300;
	float resultPosY2 = (screenHeight / 2) - 130;
	float resultPosY3 = (screenHeight / 2) - 10;
	float resultPosY4 = (screenHeight / 2) + 150;
	float resultPosY5 = (screenHeight / 2) + 220;

	float instructionsPosY = (screenHeight / 2) - 320;
	float instructionsPosY2 = (screenHeight / 2) - 280;
	float instructionsPosY3 = (screenHeight / 2) - 240;
	float instructionsPosY4 = (screenHeight / 2) - 200;
	float instructionsPosY5 = (screenHeight / 2) - 160;
	float instructionsPosY6 = (screenHeight / 2) - 120;
	float instructionsPosY7 = (screenHeight / 2) - 10;
	float instructionsPosY8 = (screenHeight / 2) + 30;
	float instructionsPosY9 = (screenHeight / 2) + 70;
	float instructionsPosY10 = (screenHeight / 2) + 180;
	float instructionsPosY11 = (screenHeight / 2) + 220;
	float instructionsPosY12 = (screenHeight / 2) + 260;
	float instructionsPosY13 = (screenHeight / 2) + 300;

	float instructions2PosY = (screenHeight / 2) - 400;
	float instructions2PosY2 = (screenHeight / 2) - 320;
	float instructions2PosY3 = (screenHeight / 2) - 230;
	float instructions2PosY4 = (screenHeight / 2) - 160;
	float instructions2PosY5 = (screenHeight / 2) - 70;
	float instructions2PosY6 = (screenHeight / 2) - 20;
	float instructions2PosY7 = (screenHeight / 2) - 20 + sizes::recHeight - sizes::recLines;
	float instructions2PosY8 = (screenHeight / 2) + 60;
	float instructions2PosY9 = (screenHeight / 2) + 150;
	float instructions2PosY10 = (screenHeight / 2) + 200;
	float instructions2PosY11 = (screenHeight / 2) + 260;
	float instructions2PosY12 = (screenHeight / 2) + 350;
	float instructions2PosY13 = (screenHeight / 2) + 400;

	float instructions3PosX = (screenWidth / 2) - texture::bomb.width - 200;
	float instructions3PosX2 = (screenWidth / 2) - 300;
	float instructions3PosX3 = (screenWidth / 2) - 310;
	float instructions3PosX4 = (screenWidth / 2) - 288;
	float instructions3PosX5 = (screenWidth / 2) + 200;
	float instructions3PosX6 = (screenWidth / 2) + 180;
	float instructions3PosX7 = (screenWidth / 2) + 170;
	float instructions3PosX8 = (screenWidth / 2) + 170;
	float instructions3PosX9 = (screenWidth / 2) - texture::colorPickerViolet.width - 200;
	float instructions3PosX10 = (screenWidth / 2) - texture::colorPickerGreen.width - 45;
	float instructions3PosX11 = (screenWidth / 2) + 45;
	float instructions3PosX12 = (screenWidth / 2) + 200;
	float instructions3PosX13 = (screenWidth / 2) - texture::tntVertical.width - 100;
	float instructions3PosX14 = (screenWidth / 2) - 230;
	float instructions3PosX15 = (screenWidth / 2) - 225;
	float instructions3PosX16 = (screenWidth / 2) + 100;
	float instructions3PosX17 = (screenWidth / 2) + 45;
	float instructions3PosX18 = (screenWidth / 2) + 40;

	float instructions3PosY = (screenHeight / 2) - 400;
	float instructions3PosY2 = (screenHeight / 2) - 300;
	float instructions3PosY3 = (screenHeight / 2) - 210;
	float instructions3PosY4 = (screenHeight / 2) - 180;
	float instructions3PosY5 = (screenHeight / 2) - 150;
	float instructions3PosY6 = (screenHeight / 2) - 50;
	float instructions3PosY7 = (screenHeight / 2) + 60;
	float instructions3PosY8 = (screenHeight / 2) + 90;
	float instructions3PosY9 = (screenHeight / 2) + 200;
	float instructions3PosY10 = (screenHeight / 2) + 300;
	float instructions3PosY11 = (screenHeight / 2) + 330;

	float pausePosY = (screenHeight / 2) - 350;
	float pausePosY2 = (screenHeight / 2) - 250;
	float pausePosY3 = (screenHeight / 2) - 300;

	float versionPosX = screenWidth / 50;
	float versionPosY = screenHeight - 40;

	float circlePosX = screenWidth / 2;
	float circlePosY = screenHeight / 20;

	float scorePosX = circlePosX - 35;
	float scorePosY = circlePosY - 10;

	float recPosX = screenHeight / 50;
	float recPosY = screenHeight / 50;

	void updateVariables()
	{
		buttonsUp = 10;
		buttonsRight = screenWidth - sizes::circleButtons - 10;
		buttonsLeft = 10;
		middleButtonsPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);

		character1PosX = screenHeight / 10;
		character1ThrowPosX = character1PosX - 10;
		character1DeadPosX = character1PosX - 20;
		character2PosX = screenWidth - screenHeight / 10 - sizes::charactersWidth;
		character2ThrowPosX = character2PosX - 10;
		character2DeadPosX = character2PosX - 50;
		if (screenHeight == 1000) characterPosY = 293;
		else if (screenHeight == 900) characterPosY = 285;
		else if (screenHeight == 800) characterPosY = 277;

		kunaiGirlInitialPos = character1ThrowPosX + 100;
		kunaiGirlFinalPos = character2ThrowPosX - 50;
		kunaiBoyInitialPos = character2ThrowPosX - 70;
		kunaiBoyFinalPos = character1ThrowPosX + 75;

		kunaiPosY = characterPosY + 80;

		titlePoxY = (screenHeight / 2) - 400;
		titlePoxY2 = (screenHeight / 2) - 300;

		menuPosY = (screenHeight / 2) - 180;
		menuPosY2 = (screenHeight / 2) - 60;
		menuPosY3 = (screenHeight / 2) + 60;
		menuPosY4 = (screenHeight / 2) + 180;
		menuPosY5 = (screenHeight / 2) + 300;

		creditsPosY = (screenHeight / 2) - 350;
		creditsPosY2 = (screenHeight / 2) - 300;
		creditsPosY3 = (screenHeight / 2) - 150;
		creditsPosY4 = (screenHeight / 2) - 100;
		creditsPosY5 = (screenHeight / 2) - 60;
		creditsPosY6 = (screenHeight / 2) - 20;
		creditsPosY7 = (screenHeight / 2) + 20;
		creditsPosY8 = (screenHeight / 2) + 60;
		creditsPosY9 = (screenHeight / 2) + 100;
		creditsPosY10 = (screenHeight / 2) + 140;
		creditsPosY11 = (screenHeight / 2) + 180;
		creditsPosY12 = (screenHeight / 2) + 220;
		creditsPosY13 = (screenHeight / 2) + 260;
		creditsPosY14 = (screenHeight / 2) + 300;
		creditsPosY15 = (screenHeight / 2) + 340;

		settingPosY = (screenHeight / 2) - 50 - sizes::buttonsHeight;
		settingPosY2 = (screenHeight / 2) + 50;

		resolutionPosY = (screenHeight / 2) - 200;
		resolutionPosY2 = (screenHeight / 2) - 120;
		resolutionPosY3 = (screenHeight / 2) - 20;
		resolutionPosY4 = (screenHeight / 2) + 80;

		audioPosY = (screenHeight / 2) - 400;
		audioPosY2 = (screenHeight / 2) - 320;
		audioPosY3 = (screenHeight / 2) - 220;
		audioPosY4 = (screenHeight / 2) - 120;
		audioPosY5 = (screenHeight / 2) + 40;
		audioPosY6 = (screenHeight / 2) + 120;
		audioPosY7 = (screenHeight / 2) + 220;
		audioPosY8 = (screenHeight / 2) + 320;

		resultPosX = (screenWidth / 2) - (texture::playAgainButton.width / 2);
		resultPosX2 = (screenWidth / 2) - (texture::playAgainButton.width / 2);
		resultPosY = (screenHeight / 2) - 300;
		resultPosY2 = (screenHeight / 2) - 130;
		resultPosY3 = (screenHeight / 2) - 10;
		resultPosY4 = (screenHeight / 2) + 150;
		resultPosY5 = (screenHeight / 2) + 220;

		instructionsPosY = (screenHeight / 2) - 320;
		instructionsPosY2 = (screenHeight / 2) - 280;
		instructionsPosY3 = (screenHeight / 2) - 240;
		instructionsPosY4 = (screenHeight / 2) - 200;
		instructionsPosY5 = (screenHeight / 2) - 160;
		instructionsPosY6 = (screenHeight / 2) - 120;
		instructionsPosY7 = (screenHeight / 2) - 10;
		instructionsPosY8 = (screenHeight / 2) + 30;
		instructionsPosY9 = (screenHeight / 2) + 70;
		instructionsPosY10 = (screenHeight / 2) + 180;
		instructionsPosY11 = (screenHeight / 2) + 220;
		instructionsPosY12 = (screenHeight / 2) + 260;
		instructionsPosY13 = (screenHeight / 2) + 300;

		instructions2PosY = (screenHeight / 2) - 400;
		instructions2PosY2 = (screenHeight / 2) - 320;
		instructions2PosY3 = (screenHeight / 2) - 230;
		instructions2PosY4 = (screenHeight / 2) - 160;
		instructions2PosY5 = (screenHeight / 2) - 70;
		instructions2PosY6 = (screenHeight / 2) - 20;
		instructions2PosY7 = (screenHeight / 2) - 20 + sizes::recHeight - sizes::recLines;
		instructions2PosY8 = (screenHeight / 2) + 60;
		instructions2PosY9 = (screenHeight / 2) + 150;
		instructions2PosY10 = (screenHeight / 2) + 200;
		instructions2PosY11 = (screenHeight / 2) + 260;
		instructions2PosY12 = (screenHeight / 2) + 350;
		instructions2PosY13 = (screenHeight / 2) + 400;

		instructions3PosX = (screenWidth / 2) - texture::bomb.width - 200;
		instructions3PosX2 = (screenWidth / 2) - 300;
		instructions3PosX3 = (screenWidth / 2) - 310;
		instructions3PosX4 = (screenWidth / 2) - 288;
		instructions3PosX5 = (screenWidth / 2) + 200;
		instructions3PosX6 = (screenWidth / 2) + 180;
		instructions3PosX7 = (screenWidth / 2) + 170;
		instructions3PosX8 = (screenWidth / 2) + 170;
		instructions3PosX9 = (screenWidth / 2) - texture::colorPickerViolet.width - 200;
		instructions3PosX10 = (screenWidth / 2) - texture::colorPickerGreen.width - 45;
		instructions3PosX11 = (screenWidth / 2) + 45;
		instructions3PosX12 = (screenWidth / 2) + 200;
		instructions3PosX13 = (screenWidth / 2) - texture::tntVertical.width - 100;
		instructions3PosX14 = (screenWidth / 2) - 230;
		instructions3PosX15 = (screenWidth / 2) - 225;
		instructions3PosX16 = (screenWidth / 2) + 100;
		instructions3PosX17 = (screenWidth / 2) + 45;
		instructions3PosX18 = (screenWidth / 2) + 40;

		instructions3PosY = (screenHeight / 2) - 400;
		instructions3PosY2 = (screenHeight / 2) - 300;
		instructions3PosY3 = (screenHeight / 2) - 210;
		instructions3PosY4 = (screenHeight / 2) - 180;
		instructions3PosY5 = (screenHeight / 2) - 150;
		instructions3PosY6 = (screenHeight / 2) - 50;
		instructions3PosY7 = (screenHeight / 2) + 60;
		instructions3PosY8 = (screenHeight / 2) + 90;
		instructions3PosY9 = (screenHeight / 2) + 200;
		instructions3PosY10 = (screenHeight / 2) + 300;
		instructions3PosY11 = (screenHeight / 2) + 330;

		pausePosY = (screenHeight / 2) - 350;
		pausePosY2 = (screenHeight / 2) - 250;
		pausePosY3 = (screenHeight / 2) - 300;

		versionPosX = screenWidth / 50;
		versionPosY = screenHeight - 40;

		circlePosX = screenWidth / 2;
		if (screenHeight == 1000)
		{
			circlePosY = screenHeight / 20;
			scorePosX = circlePosX - 35;
		}
		else if (screenHeight == 900)
		{
			circlePosY = screenHeight / 15;
			scorePosX = circlePosX - 30;
		}
		else if (screenHeight == 800)
		{
			circlePosY = screenHeight / 13;
			scorePosX = circlePosX - 25;
		}

		scorePosY = circlePosY - 10;

		recPosX = screenHeight / 50;
		recPosY = screenHeight / 50;
	}
}